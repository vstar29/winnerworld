package com.winnersworlwide.interfaces;

public interface RequestDoneInterface {
    void onRequestDone(String response);
}
