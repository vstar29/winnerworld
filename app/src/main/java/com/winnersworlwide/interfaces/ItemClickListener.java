package com.winnersworlwide.interfaces;


import android.view.View;

/**
 * Created by VICTOR on 05-Aug-17.
 */
public interface ItemClickListener {
    void onClick(View view, int position);
}
