package com.winnersworlwide.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by PC-i3 on 6/16/2017.
 */

public class Efra_Regular extends TextView {
    public Efra_Regular(Context context) {
        super(context);
        init();
    }

    public Efra_Regular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Efra_Regular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/effra_etd_rg.ttf");
            setTypeface(tf);
        }
    }
}
