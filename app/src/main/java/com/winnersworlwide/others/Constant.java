package com.winnersworlwide.others;

public class Constant {

    public static final String YOUTUBE_API_KEY = "AIzaSyD9TJIkaPkIoAIlzrcSel0UY61sS3LuBDk"; //"AIzaSyCiEefznUwVulsIqVy6DeoSQZgsGJgCsDM";
    public static final String BASE_URL = "http://13.93.33.32/WinnersWorldService/api/";
    public static final String ONE_SIGNAL_KEY = "7f24c911-b934-41c5-8d31-6c9ea6fbe8b5";
    public static final String API_NAME = "";
    public static final String API_KEY = "";
    public static final String CHANNEL_ID = "UCyUKtrMdDilf74SPkCCKKtw";
    public static final String FCM_CHANNEL_ID ="WinnersFCMCHANNELID";
    public static final String RADIO_NOTIFY_ID ="RADIONOTIFYID";
    public static final String STREAM_EVENT ="STREAM_EVENT";
    public static final String LAST_NOTIFY ="LAST_NOTIFY";

    //Api endpoints
    public static final String ADD_TESTIMONY = "Testimonies/AddTestimony";
    public static final String GET_TESTIMONIES = "Testimonies/GetTestimonies";
    public static final String GET_ANNOUNCEMENTS = "Announcements/GetAnnouncements";
    public static final String GET_ANNOUNCEMENT = "Announcements/FindAnnouncementById/";
    public static final String GET_NOTIFICATIONS = "PushNotifications/GetNotifications";
    public static  final String GET_DASHBOARD_CONTENT = "Dashboard/GetDashboardData";
    public static final String ADD_USER = "Users/AddUser";
    public static  final  String INCREASE_VIEWS = "Testimonies/IncrementViews";
    public static final int LIMIT =  20;



    public static final String TESTIMONY = "TESTIMONY";
    public static final String ANNOUNCEMENT = "ANNOUNCEMENT";
    public static final String NOTIFICATION = "NOTIFICATION";
    public static final String FRESHER = "FRESHER";
    public static final String LIVE_ON = "LIVE_ON";
    public static final String LIVE_ID = "LIVE_ID";
    public static final String LIVE_TITLE = "LIVE_TITLE";
    public static final String DEVICE_ID = "DEVICE_ID";
    public static final String DASH_CONTENT = "DASH_CONTENT";

    public static final String ONLINE_GIVING_URL = "https://give.domi.org.ng/";
    public static final String DOWNLOADS_URL = "http://faithtabernacle.org.ng/downloads/";
    public static final String ONLINE_BOOKSTORE_URL = "http://domionlinestore.org/";
    public static final String DOMI_RADIO = "http://radio.shoutcastmedia.net:8302/stream";

    public static final String DOWNLOADS = "DOWNLOADS";
    public static final String ONLINE_GIVING = "ONLINE GIVING";
    public static final String ONLINE_BOOKSTORE = "BOOKSTORE";
    public static  final String APP_VERSION ="1.0";

    public static final String PLAYSTORE_URL = "https://play.google.com/store/apps/details?id=com.winnersworlwide";
}
