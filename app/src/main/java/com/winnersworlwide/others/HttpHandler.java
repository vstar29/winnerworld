package com.winnersworlwide.others;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.winnersworlwide.activities.SplashActivity;
import com.winnersworlwide.interfaces.RequestDoneInterface;

import java.util.Map;

/**
 * Created by JUMIA-1888 on 2/20/2018.
 */

public class HttpHandler {
    private static RequestQueue requestQueue;
    String uri;
    private Context context;

    public HttpHandler(Context context) {
        this.context = context;
    }


    /*---make http request to the server--*/
    public void makePost(String url, final Map<String, String> params, final RequestDoneInterface callback) {

        uri = url.trim();
        url = Constant.BASE_URL + url;
        url = url.trim();
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("dd", "vibe_:" + response);
                        if (!uri.equalsIgnoreCase(Constant.ADD_USER)) {
                            callback.onRequestDone(response);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //callback.onRequestDone(error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                params.put("api_name", Constant.API_NAME);
                params.put("api_key", Constant.API_KEY);
                params.put("user_device", Helper.getAndroidVersion());
                params.put("device_id", Helper.getDeviceID());

                return params;
            }
        };
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 35000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 35000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        requestQueue.add(postRequest);

    }


    public void makeGet(String url, final RequestDoneInterface callback) {

        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);

        }

        StringRequest postRequest = new StringRequest(Request.Method.GET, url.trim(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onRequestDone(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                            error.printStackTrace();
                    }
                }
        ) {

        };

        postRequest.setShouldCache(false);
        requestQueue.add(postRequest);


    }

    public void makeGet(String url, final RequestDoneInterface callback,Boolean cache) {

        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);

        }

        StringRequest postRequest = new StringRequest(Request.Method.GET, url.trim(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onRequestDone(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();
                    }
                }
        ) {

        };

        postRequest.setShouldCache(cache);
        requestQueue.add(postRequest);


    }





    //Multi part request
    public void multiPartForm(String url, final Map<String, String> params, final RequestDoneInterface callback) {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
        url = Constant.BASE_URL + url;
        url = url.trim();
        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        callback.onRequestDone(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        for (Map.Entry<String,String> pair : params.entrySet()) {
            if(pair.getKey().equalsIgnoreCase("Attachment"))
            {
                request.addFile(pair.getKey(), pair.getValue());
            }
            else
            {
                request.addMultipartParam(pair.getKey(), "text/plain", pair.getValue());
            }


        }
        requestQueue.add(request);
    }


}
