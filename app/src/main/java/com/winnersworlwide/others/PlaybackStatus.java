package com.winnersworlwide.others;

public enum PlaybackStatus {

    PLAYING,
    PAUSED
}
