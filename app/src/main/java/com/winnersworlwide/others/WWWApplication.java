package com.winnersworlwide.others;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import com.google.gson.Gson;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.winnersworlwide.activities.AnnouncementDetailsActivity;
import com.winnersworlwide.activities.MainActivity;
import com.winnersworlwide.activities.TestimonyActivity;

import org.json.JSONException;
import org.json.JSONObject;

import static com.onesignal.OneSignal.OSInFocusDisplayOption.Notification;

public class WWWApplication extends Application {
    private static WWWApplication mInstance;

    public static synchronized WWWApplication getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(new ConnectivityReceiver(), intentFilter);

        OneSignal.startInit(this)
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .setNotificationOpenedHandler(new NotificationOpenedHandler())
                .inFocusDisplaying(Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        super.onCreate();
        mInstance = this;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;

    }

    private class NotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {


        }
    }


    private class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        // This fires when a notification is opened by tapping on it.
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {

            JSONObject data = result.notification.payload.additionalData;
            Gson gson = new Gson();
            String notificationObject = "EMPTY";
            try {
                notificationObject = data.getString("notificationObject");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            final com.winnersworlwide.models.Notification notification = gson.fromJson(notificationObject, com.winnersworlwide.models.Notification.class);
            Object activityToLaunch;
            switch (notification.getDestination()) {
                case Constant.TESTIMONY:
                    activityToLaunch = TestimonyActivity.class;
                    break;
                case Constant.ANNOUNCEMENT:
                    activityToLaunch = AnnouncementDetailsActivity.class;

                    break;
                default:
                    activityToLaunch = MainActivity.class;
                    break;

            }
            Intent intent = new Intent(WWWApplication.getAppContext(), (Class<?>) activityToLaunch);
            intent.putExtra(Constant.NOTIFICATION, notification.getDestinationID());
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);


        }
    }

}
