package com.winnersworlwide.others;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Pattern;

public class Helper {


    /*--get date from String--*/
    public static Date getDate(String dateString, String dateFormat) {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        try {
            date = format.parse(dateString);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;


    }

    /*--format date string to desire format--*/

    public static String formatTime(String timeString, String pattern) {

        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat outputFormat = new SimpleDateFormat(pattern);
            Date date = inputFormat.parse(timeString);
            String formattedDate = outputFormat.format(date);
            return formattedDate;
        } catch (Exception e) {

            e.printStackTrace();
        }

        return "";

    }

    public static String formatTime(Date date, String pattern) {

        try {
            DateFormat dateFormat = new SimpleDateFormat(pattern);
            return dateFormat.format(date);
        } catch (Exception e) {

            e.printStackTrace();
        }

        return "";

    }

    /*--save user settings/preference--*/
    public static void savePreference(Context context, String key, String value) {
        SharedPreferences appSharedPref = context.getSharedPreferences("My_Preference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = appSharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /*--get user settings/preference--*/
    public static String getPreference(Context context, String key) {
        SharedPreferences appSharedPref = context.getSharedPreferences("My_Preference", Context.MODE_PRIVATE);
        return appSharedPref.getString(key, "");

    }

    /*--save item price to preference--*/
    public static void deleteFromPreference(Context context, String key) {

        SharedPreferences appSharedPref = context.getSharedPreferences("My_Preference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = appSharedPref.edit();
        editor.remove(key);
        editor.commit();

    }

    public synchronized static String getDeviceID() {
        String deviceID = getPreference(WWWApplication.getAppContext(), Constant.DEVICE_ID);
        if (TextUtils.isEmpty(deviceID)) {
            deviceID = UUID.randomUUID().toString();

        }
        savePreference(WWWApplication.getAppContext(), Constant.DEVICE_ID, deviceID);
        return deviceID;
    }

    public static String getAndroidVersion() {
        String OSVersion = "Android " + Build.VERSION.RELEASE + " SDK : " + Build.VERSION.SDK_INT;
        return OSVersion;
    }


    public static Boolean isValidName(String name)
    {
        if(TextUtils.isEmpty(name))
        {
            return  false;
        }

        return !Pattern.compile( "[0-9]" ).matcher( name ).find();
    }


    /*-@param email{String}
     "response {boolean}--*/
    public static boolean isValidEmail(String email)
    {
        if(TextUtils.isEmpty(email))
        {
            return  false;
        }
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }


}
