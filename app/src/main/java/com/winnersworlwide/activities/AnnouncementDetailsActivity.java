package com.winnersworlwide.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.RequestDoneInterface;
import com.winnersworlwide.models.Announcement;
import com.winnersworlwide.others.ConnectivityReceiver;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.HttpHandler;
import com.winnersworlwide.others.WWWApplication;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;

public class AnnouncementDetailsActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    TextView navigationTitle;
    TextView announcementDate, announcementTitle, aAnnouncement;
    Announcement announcement;
    RelativeLayout navLeftView;
    RelativeLayout featuredImage;
    Button viewpdf;
    Snackbar snackbar;
    ProgressDialog progressDialog;
    Boolean connected = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement_details);

        navLeftView = findViewById(R.id.toolbarLeftView);
        navigationTitle = findViewById(R.id.toolbarTitle);
        viewpdf = findViewById(R.id.viewpdf);
        navigationTitle.setText("Announcements");
        announcementDate = findViewById(R.id.date);
        announcementTitle = findViewById(R.id.title);
        featuredImage = findViewById(R.id.image);
        aAnnouncement = findViewById(R.id.announcement);
        aAnnouncement.setMovementMethod(new ScrollingMovementMethod());
        Intent intent = getIntent();
        connected = checkConnection();
        progressDialog  = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Loading");
        if(intent.hasExtra(Constant.NOTIFICATION))
        {

            getAnnouncement(intent.getIntExtra(Constant.NOTIFICATION,0));
        }
        else
        {
            announcement = intent.getParcelableExtra(Constant.ANNOUNCEMENT);
            populateView();
        }


        navLeftView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getIntent().hasExtra(Constant.NOTIFICATION))
                {

                    goNotification(view);
                }
                else
                {
                    goAnnouncement(view);
                }

            }
        });

    }

    public  void populateView()
    {
        announcementDate.setText(announcement.getFormattedDate() );
        announcementTitle.setText(announcement.getTitle());
        aAnnouncement.setText(announcement.getDescription());
        if (!TextUtils.isEmpty(announcement.getFeaturedImage())) {

        }
        if (TextUtils.isEmpty(announcement.getAttachment())) {
            viewpdf.setVisibility(View.GONE);
        } else {
            viewpdf.setVisibility(View.VISIBLE);
        }

    }

    public void goParent(View v) {
        goAnnouncement(v);
    }

    public void goAnnouncement(View v) {

        Intent intent = new Intent(this, AnnouncementActivity.class);
        startActivity(intent);
    }
    public void goNotification(View v)
    {
        Intent intent = new Intent(this, NotificationActivity.class);
        startActivity(intent);
    }


    public void viewPDF(View v) {
        if (!TextUtils.isEmpty(announcement.getAttachment())) {
            File file = new File(announcement.getAttachment());
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        WWWApplication.getInstance().setConnectivityListener(this);
    }



    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected = isConnected;

    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
        return isConnected;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(new ConnectivityReceiver());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showSnack(boolean isConnected) {

        if (isConnected) {
            try {
                snackbar.dismiss();

            } catch (Exception e) {

            }

        }
        if (!isConnected) {

            final CoordinatorLayout coordinatorLayout = findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "Internet connection error", Snackbar.LENGTH_INDEFINITE);
            View snackView = snackbar.getView();
            snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.cancelled));
            TextView tv = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }


    public void getAnnouncement(int ID) {

        if (connected) {


            HttpHandler httpHandler = new HttpHandler(this);
                progressDialog.show();
                httpHandler.makeGet(Constant.BASE_URL + Constant.GET_ANNOUNCEMENT + ID, new RequestDoneInterface() {
                    @Override
                    public void onRequestDone(String response) {
                        Gson Converter = new Gson();
                        Type type = new TypeToken<List<Announcement>>() {
                        }.getType();
                        try {

                                announcement = Converter.fromJson(response, Announcement.class);
                               populateView();


                        } catch (Exception e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                });


        } else {
           showSnack(false);
        }

    }
}
