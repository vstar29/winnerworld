package com.winnersworlwide.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Parcelable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.winnersworlwide.R;
import com.winnersworlwide.adapters.NotificationAdapter;
import com.winnersworlwide.interfaces.ItemClickListener;
import com.winnersworlwide.interfaces.RequestDoneInterface;
import com.winnersworlwide.models.Notification;
import com.winnersworlwide.others.ConnectivityReceiver;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.Helper;
import com.winnersworlwide.others.HttpHandler;
import com.winnersworlwide.others.WWWApplication;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener, ItemClickListener {

    private static final String M_STATE = "no_state_o";
    Boolean connected;
    TextView navigationTitle;
    NotificationAdapter notificationAdapter;
    Boolean fetching = false;
    RecyclerView recyclerView;
    ProgressBar loadMore;
    ProgressBar progressBar;
    RecyclerView.LayoutManager layoutManager;
    Parcelable recyclerState;
    RelativeLayout errorPage;
    LinearLayout mainContent;
    Snackbar snackbar;
    private int offset = 0;
    private int limit = Constant.LIMIT;
    private ArrayList<Notification> notifications;
    RelativeLayout navLeftView;
    Button leftIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        navigationTitle = findViewById(R.id.toolbarTitle);
        navLeftView = findViewById(R.id.toolbarLeftView);
        navigationTitle.setText("Notifications");
        connected = checkConnection();
        progressBar = findViewById(R.id.progressBar);
        mainContent = findViewById(R.id.main_content);
        errorPage = findViewById(R.id.connection_error);
        loadMore = findViewById(R.id.loading);
        notifications = new ArrayList<>();
        leftIcon = findViewById(R.id.leftIcon);
        getNotifications();
        progressBar.setVisibility(View.VISIBLE);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int lastVisibleItem, totalItemCount;
                    totalItemCount = linearLayoutManager.getItemCount();
                    int visibleThreshold = 2;
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (totalItemCount >= limit) {
                        if (!fetching && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            loadMore.setVisibility(View.VISIBLE);
                            getNotifications();
                        }
                    }
                }

            }
        });
        navLeftView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goMain(view);

            }
        });
    }

    public void goParent(View v) {
        goMain(v);
    }

    public void goMain(View v) {

        goMain();
    }

    public void goMain() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (recyclerState != null) {
            layoutManager.onRestoreInstanceState(recyclerState);
        }
        WWWApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        recyclerState = layoutManager.onSaveInstanceState();
        state.putParcelable(M_STATE, recyclerState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if (state != null) {
            recyclerState = state.getParcelable(M_STATE);
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected = isConnected;
        mainContent.setVisibility(View.VISIBLE);
        errorPage.setVisibility(View.INVISIBLE);
    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
        return isConnected;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(new ConnectivityReceiver());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showSnack(boolean isConnected) {

        if (isConnected) {
            try {
                snackbar.dismiss();

            } catch (Exception e) {

            }

        }
        if (!isConnected) {

            final CoordinatorLayout coordinatorLayout = findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "Internet connection error", Snackbar.LENGTH_INDEFINITE);
            View snackView = snackbar.getView();
            snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.cancelled));
            TextView tv = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    public void getNotifications() {


        notificationAdapter = new NotificationAdapter(NotificationActivity.this, notifications);
        recyclerView.setAdapter(notificationAdapter);
        notificationAdapter.setClickListener(NotificationActivity.this);
       if (connected) {

            Map<String, String> params = new HashMap<>();
            params.put("offset", Integer.toString(offset));
            HttpHandler httpHandler = new HttpHandler(this);
            if (!fetching) {
                fetching = true;

                httpHandler.makeGet(Constant.BASE_URL + Constant.GET_NOTIFICATIONS+ "?offset=" + offset + "&limit=" + limit, new RequestDoneInterface() {
                    @Override
                    public void onRequestDone(String response) {
                        Gson Converter = new Gson();
                        Type type = new TypeToken<List<Notification>>() {
                        }.getType();
                        fetching = false;
                        try {

                            if (offset == 0) {
                                notifications = Converter.fromJson(response, type);
                                notificationAdapter = new NotificationAdapter(NotificationActivity.this, notifications);
                                recyclerView.setAdapter(notificationAdapter);
                                notificationAdapter.setClickListener(NotificationActivity.this);
                                Helper.savePreference(NotificationActivity.this,Constant.LAST_NOTIFY, String.valueOf(notifications.get(0).getId()));

                            } else {
                                loadMore.setVisibility(View.INVISIBLE);
                                ArrayList<Notification> holder = Converter.fromJson(response, type);
                                notifications.addAll(holder);
                                notificationAdapter.notifyDataSetChanged();
                                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                                recyclerView.smoothScrollToPosition(linearLayoutManager.getItemCount() - holder.size());
                            }
                            offset += Constant.LIMIT;
                        } catch (Exception e) {
                            progressBar.setVisibility(View.INVISIBLE);
                            loadMore.setVisibility(View.INVISIBLE);
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });

            }
        } else {
            mainContent.setVisibility(View.INVISIBLE);
            errorPage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view, int position) {

        //open details base on type
        Notification notification = notifications.get(position);
        Intent intent = new Intent(this, AnnouncementDetailsActivity.class);
        intent.putExtra(Constant.NOTIFICATION, notification.getDestinationID());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

        goMain();

    }

}
