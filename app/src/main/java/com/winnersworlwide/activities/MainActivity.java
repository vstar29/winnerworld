package com.winnersworlwide.activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.winnersworlwide.R;
import com.winnersworlwide.adapters.DashAdapter;
import com.winnersworlwide.adapters.DrawerItemAdapter;
import com.winnersworlwide.adapters.MonthlyBookAdapter;
import com.winnersworlwide.interfaces.ItemClickListener;
import com.winnersworlwide.models.DashBoardContent;
import com.winnersworlwide.models.DashItem;
import com.winnersworlwide.models.MonthlyBook;
import com.winnersworlwide.models.MonthlyTheme;
import com.winnersworlwide.others.ConnectivityReceiver;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.Helper;
import com.winnersworlwide.others.WWWApplication;
import com.winnersworlwide.services.RadioService;
import com.winnersworlwide.services.StreamSynchService;

import java.util.ArrayList;


public class MainActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, ConnectivityReceiver.ConnectivityReceiverListener, ItemClickListener {

    private static final int RECOVERY_REQUEST = 1;
    private static final String M_STATE = "h_state_o";
    RecyclerView.LayoutManager layoutManager;
    Parcelable recyclerState;
    Boolean connected;
    Boolean fabMenuOn = false;
    DrawerLayout drawerLayout;
    GridView gridview;
    RelativeLayout closeDrawer;
    TextView navigationTitle;
    Snackbar snackbar;
    ImageView noLiveStream;
    RelativeLayout navLeftView, navRightView;
    LinearLayout shareDiv;
    PopupWindow themeOfTheMonthPopup;
    View themeOfTheMonthLayout;
    Gson gson;
    RecyclerView booksView;
    Boolean serviceBound = false;
    Boolean fullScrean = false;
    private RadioService radioPlayer;
    private YouTubePlayerView youTubeView;
    private String liveVideoId = "";
    private String liveVideoTitle = "";
    private TextView liveTitle;
    private RecyclerView recyclerView;
    private ArrayList<DashItem> dashItems;
    private RelativeLayout fabMenuView;
    private TextView featuredVideo;
    private TextView themeTitle, theme, verse;
    private MonthlyTheme monthlyTheme;
    private ArrayList<MonthlyBook> monthlyBooks;
    private DashBoardContent dashBoardContent;
    private LinearLayout notifyNew;
    private ImageView shareIcon, radioIcon;
    TextView extraTitle;
    private YouTubePlayer youTubePlayer;
    //Binding this Client to the RadioService Service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            RadioService.LocalBinder binder = (RadioService.LocalBinder) service;
            radioPlayer = binder.getService();

            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };
    /**
     * Broadcast receiver to receive the data
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (!TextUtils.isEmpty(intent.getStringExtra(Constant.LIVE_ID))) {
                liveVideoTitle = intent.getStringExtra(Constant.LIVE_TITLE);
                liveVideoId = intent.getStringExtra(Constant.LIVE_ID);
                //update lIvestream
                youTubePlayer.cueVideo(liveVideoId);
                setUpLiveStream();
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        youTubeView = findViewById(R.id.youtube_view);
        recyclerView = findViewById(R.id.recyclerView);
        gson = new Gson();
        drawerLayout = findViewById(R.id.drawer_layout);
        youTubeView.initialize(Constant.YOUTUBE_API_KEY, this);
        liveTitle = findViewById(R.id.title);
        fabMenuView = findViewById(R.id.fab_menu_view);
        featuredVideo = findViewById(R.id.featured_video);
        fabMenuView.setVisibility(View.INVISIBLE);
        gridview = findViewById(R.id.gridView);
        closeDrawer = findViewById(R.id.toolbarLeftView);
        navigationTitle = findViewById(R.id.toolbarTitle);
        notifyNew = findViewById(R.id.notify_new);
        noLiveStream = findViewById(R.id.no_live_stream);
        navLeftView = findViewById(R.id.leftView);
        navRightView = findViewById(R.id.toolbarrightView);
        navRightView.setVisibility(View.VISIBLE);
        navigationTitle.setText(R.string.menu);
        DrawerItemAdapter drawerItemAdapter = new DrawerItemAdapter(this);
        gridview.setAdapter(drawerItemAdapter);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        connected = checkConnection();
        shareDiv = findViewById(R.id.sharediv);
        liveVideoId = Helper.getPreference(this, Constant.LIVE_ID);
        liveVideoTitle = Helper.getPreference(this, Constant.LIVE_TITLE);
        String dashContent = Helper.getPreference(this, Constant.DASH_CONTENT);
        dashBoardContent = gson.fromJson(dashContent, DashBoardContent.class);
        monthlyBooks = dashBoardContent.getBooksofTheMonth();
        monthlyTheme = dashBoardContent.getThemeOfTheMonth();

        shareIcon = findViewById(R.id.share);
        radioIcon = findViewById(R.id.radio);
        extraTitle = findViewById(R.id.extra_title);

        prepareDashBoardContent();
        DashAdapter dashAdapter = new DashAdapter(this, dashItems);
        recyclerView.setAdapter(dashAdapter);
        dashAdapter.setClickListener(this);
        LayoutInflater layoutinflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        themeOfTheMonthLayout = layoutinflater.inflate(R.layout.theme_of_the_month_pop_up, null, false);
        themeTitle = themeOfTheMonthLayout.findViewById(R.id.month);
        theme = themeOfTheMonthLayout.findViewById(R.id.theme);
        verse = themeOfTheMonthLayout.findViewById(R.id.verse);
        booksView = themeOfTheMonthLayout.findViewById(R.id.booksView);
        booksView.setHasFixedSize(true);
        RecyclerView.LayoutManager layManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        booksView.setLayoutManager(layManager);

        navLeftView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDrawer(view);
            }
        });
        closeDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
            }
        });

        setUpLiveStream();


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                switch (i) {
                    case 0:
                        Intent intent0 = new Intent(MainActivity.this, BrowserActivity.class);
                        intent0.putExtra("url", Constant.DOWNLOADS_URL);
                        intent0.putExtra("title", Constant.DOWNLOADS);
                        startActivity(intent0);
                        break;

                    case 1:

                        Intent intent1 = new Intent(MainActivity.this, RadioActivity.class);
                        startActivity(intent1);
                        break;

                    case 2:
                        Intent intent2 = new Intent(MainActivity.this, BrowserActivity.class);
                        intent2.putExtra("url", Constant.ONLINE_GIVING_URL);
                        intent2.putExtra("title", Constant.ONLINE_GIVING);
                        startActivity(intent2);
                        break;
                    case 3:
                        Intent intent3 = new Intent(MainActivity.this, TestimonyActivity.class);
                        startActivity(intent3);
                        break;

                    case 4:
                        Intent intent4 = new Intent(MainActivity.this, BrowserActivity.class);
                        intent4.putExtra("url", Constant.ONLINE_BOOKSTORE_URL);
                        intent4.putExtra("title", Constant.ONLINE_BOOKSTORE);
                        startActivity(intent4);
                        break;

                    case 5:
                        /*Intent intent5 = new Intent(MainActivity.this, ContactActivity.class);
                        startActivity(intent5);
                        break;*/
                        Intent intent5 = new Intent(MainActivity.this, AnnouncementActivity.class);
                        startActivity(intent5);
                        break;


                }
            }
        });

        //books of the month set up
        preparedThemePopUp();


    }

    public void goParent(View v) {
        closeDrawer();
    }

    @Override
    public void onClick(View view, int position) {

        switch (position) {

            case 0:
                showThemeOfTheMonthPopUp();
                break;
            case 1:
                Intent intent1 = new Intent(this, TestimonyActivity.class);
                startActivity(intent1);
                break;

            case 2:

                Intent intent = new Intent(this, AnnouncementActivity.class);
                startActivity(intent);
                break;

            default:

        }

    }

    public void goPlayStore(View v) {

    }

    public void exitApp(View v) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        // Otherwise defer to system default behavior.
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onInitializationSuccess(Provider provider, final YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            this.youTubePlayer = player;
            liveTitle.setText(liveVideoTitle);
            player.cueVideo(liveVideoId.trim());
            startSreamSynch();
            player.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
                @Override
                public void onPlaying() {
                    if (!fullScrean) {
                        player.setFullscreen(true);
                        fullScrean = true;
                    }

                }

                @Override
                public void onPaused() {

                }

                @Override
                public void onBuffering(boolean b) {

                }

                @Override
                public void onSeekTo(int i) {

                }

                @Override
                public void onStopped() {

                }
            });


        }
    }

    public void openDrawer(View v) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        }
    }

    public void shareVideo(View v) {
        if (!TextUtils.isEmpty(liveVideoId)) {
            String url = "https://www.youtube.com/watch?v=" + liveVideoId;
            String str = Uri.parse(url).toString();
            String msg = liveVideoTitle;
            Intent localIntent = new Intent("android.intent.action.SEND");
            localIntent.setType("text/plain");
            localIntent.putExtra("android.intent.extra.TEXT", msg + " " + ":" + str);
            startActivity(Intent.createChooser(localIntent, "Share via"));
        }

    }

    public void goMain(View v) {
        toggleFloatMenu(v);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        recyclerState = layoutManager.onSaveInstanceState();
        state.putParcelable(M_STATE, recyclerState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if (state != null)
            recyclerState = state.getParcelable(M_STATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (recyclerState != null) {
            layoutManager.onRestoreInstanceState(recyclerState);
        }

        WWWApplication.getInstance().setConnectivityListener(this);

        if (!serviceBound && TextUtils.isEmpty(Helper.getPreference(this, Constant.LIVE_ID))) {
            Intent playerIntent = new Intent(this, RadioService.class);
            startService(playerIntent);
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected = isConnected;
    }

    public void startSreamSynch() {
        if (TextUtils.isEmpty(liveVideoId)) {
            startService(new Intent(this, StreamSynchService.class));
            LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(Constant.STREAM_EVENT));
        }
    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
        return isConnected;
    }

    public void showSnack(boolean isConnected) {

        if (isConnected) {
            try {
                snackbar.dismiss();

            } catch (Exception e) {

            }

        }
        if (!isConnected) {

            final CoordinatorLayout coordinatorLayout = findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "Internet connection error", Snackbar.LENGTH_INDEFINITE);
            View snackView = snackbar.getView();
            snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.cancelled));
            TextView tv = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(new ConnectivityReceiver());
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (serviceBound) {
            unbindService(serviceConnection);

        }

    }

    public void toggleFloatMenu(View v) {

        if (!fabMenuOn) {
            showFloatMenu();
            fabMenuOn = !fabMenuOn;
        } else {
            hideFloatMenu();
            fabMenuOn = !fabMenuOn;
        }
    }

    public void showFloatMenu() {

        fabMenuView.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                fabMenuView.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(400);
        animate.setFillAfter(true);
        fabMenuView.startAnimation(animate);
    }

    public void hideFloatMenu() {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                fabMenuView.getHeight()); // toYDelta
        animate.setDuration(400);
        animate.setFillAfter(true);
        fabMenuView.startAnimation(animate);

    }

    public void goTestimony(View v) {
        Intent intent = new Intent(this, TestimonyActivity.class);
        startActivity(intent);
    }

    public void goAnnouncement(View v) {

        Intent intent = new Intent(this, AnnouncementActivity.class);
        startActivity(intent);
    }

    public void goDownloads(View v) {
        Intent intent0 = new Intent(MainActivity.this, BrowserActivity.class);
        intent0.putExtra("url", Constant.DOWNLOADS_URL);
        intent0.putExtra("title", "DOWNLOADS");
        startActivity(intent0);
    }

    public void goNotification(View v) {
        Intent intent = new Intent(this, NotificationActivity.class);
        startActivity(intent);
    }
    public void goRadio(View v) {
        Intent intent = new Intent(this, RadioActivity.class);
        startActivity(intent);
    }

    public void goBookstore(View v) {
        Intent intent4 = new Intent(MainActivity.this, BrowserActivity.class);
        intent4.putExtra("url", Constant.ONLINE_BOOKSTORE_URL);
        intent4.putExtra("title", Constant.ONLINE_BOOKSTORE);
        startActivity(intent4);
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            // Otherwise defer to system default behavior.
            super.onBackPressed();

        }


    }

    public void prepareDashBoardContent() {
        dashItems = new ArrayList<>();
        dashItems.add(new DashItem("Theme Of The Month", monthlyTheme.getPeriod() + " - " + monthlyTheme.getTheme(), R.drawable.ic_word_of_today, monthlyTheme.getScripture()));
        dashItems.add(new DashItem("Testimonies", "Testify to the goodness of the lord in your life. Share your testimony with brethren and you will be blessed", R.drawable.ic_testimonies, ""));
        dashItems.add(new DashItem("Announcements", "View the bank details of the church for those who want to give to the lord. Get other important information at this section", R.drawable.ic_announcement, ""));
        if (Helper.getPreference(this, Constant.LAST_NOTIFY).equalsIgnoreCase(String.valueOf(dashBoardContent.getLastNotificationId()))) {
            notifyNew.setVisibility(View.INVISIBLE);
        } else {
            notifyNew.setVisibility(View.VISIBLE);
        }

    }

    public void preparedThemePopUp() {
        MonthlyBookAdapter monthlyBookAdapter = new MonthlyBookAdapter(this, monthlyBooks);
        booksView.setAdapter(monthlyBookAdapter);
        theme.setText(monthlyTheme.getTheme());
        verse.setText(monthlyTheme.getScripture());
    }

    public void showThemeOfTheMonthPopUp() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        themeOfTheMonthPopup = new PopupWindow(themeOfTheMonthLayout, width, height, true);
        themeOfTheMonthPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        themeOfTheMonthPopup.setFocusable(true);
        themeOfTheMonthPopup.setOutsideTouchable(true);
        themeTitle.setText(monthlyTheme.getTitle());

        themeOfTheMonthPopup.showAtLocation(themeOfTheMonthLayout, Gravity.CENTER, 0, 0);

    }


    public void closeThemeDetails(View v) {
        themeOfTheMonthPopup.dismiss();
    }

    public void setUpLiveStream() {
        if (TextUtils.isEmpty(liveVideoId)) {
            youTubeView.setVisibility(View.GONE);
            noLiveStream.setVisibility(View.VISIBLE);
            //shareDiv.setVisibility(View.INVISIBLE);
            shareIcon.setVisibility(View.INVISIBLE);
            radioIcon.setVisibility(View.VISIBLE);
            extraTitle.setText("Radio");
            featuredVideo.setText(R.string.no_live_stream);

        } else {
            youTubeView.setVisibility(View.VISIBLE);
            noLiveStream.setVisibility(View.GONE);
            shareDiv.setVisibility(View.VISIBLE);
            featuredVideo.setText(R.string.live_now);
            extraTitle.setText("Share");
            shareIcon.setVisibility(View.VISIBLE);
            radioIcon.setVisibility(View.INVISIBLE);

        }
    }


}
