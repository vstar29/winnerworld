package com.winnersworlwide.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.winnersworlwide.R;
import com.winnersworlwide.adapters.DrawerItemAdapter;
import com.winnersworlwide.adapters.TestimonyAdapter;
import com.winnersworlwide.interfaces.ItemClickListener;
import com.winnersworlwide.interfaces.RequestDoneInterface;
import com.winnersworlwide.models.DashBoardContent;
import com.winnersworlwide.models.Testimony;
import com.winnersworlwide.others.ConnectivityReceiver;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.Helper;
import com.winnersworlwide.others.HttpHandler;
import com.winnersworlwide.others.WWWApplication;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class TestimonyActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener, ItemClickListener {


    private static final String M_STATE = "te_state_o";
    private static final int ADD_TESTIMONY_REQUEST = 2189;
    Boolean connected;
    Boolean fabMenuOn = false;
    DrawerLayout drawerLayout;
    GridView gridview;
    RelativeLayout menuIcon;
    RelativeLayout closeDrawer;
    TextView navigationTitle;
    TestimonyAdapter testimonyAdapter;
    Boolean fetching = false;
    RecyclerView recyclerView;
    ProgressBar loadMore;
    ProgressBar progressBar;
    RecyclerView.LayoutManager layoutManager;
    Parcelable recyclerState;
    RelativeLayout errorPage;
    LinearLayout mainContent;
    Snackbar snackbar;
    ImageView fabMenu;
    RelativeLayout navRightView;
    private RelativeLayout fabMenuView;
    private int offset = 0;
    private int limit = Constant.LIMIT;
    private ArrayList<Testimony> testimonies;
    LinearLayout notifyNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testimony);
        fabMenuView = findViewById(R.id.fab_menu_view);
        fabMenu = findViewById(R.id.fab_menu);
        fabMenuView.setVisibility(View.INVISIBLE);
        gridview = findViewById(R.id.gridView);
        menuIcon = findViewById(R.id.leftView);
        closeDrawer = findViewById(R.id.toolbarLeftView);
        navigationTitle = findViewById(R.id.toolbarTitle);
        navigationTitle.setText(R.string.menu);
        drawerLayout = findViewById(R.id.drawer_layout);
        connected = checkConnection();
        progressBar = findViewById(R.id.progressBar);
        mainContent = findViewById(R.id.main_content);
        errorPage = findViewById(R.id.connection_error);
        loadMore = findViewById(R.id.loading);
        DrawerItemAdapter drawerItemAdapter = new DrawerItemAdapter(this);
        gridview.setAdapter(drawerItemAdapter);
        recyclerView = findViewById(R.id.recyclerView);
        navRightView = findViewById(R.id.toolbarrightView);
        navRightView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        progressBar.setVisibility(View.VISIBLE);
        getTestimonies();
        notifyNew = findViewById(R.id.notify_new);
        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                openDrawer(view);
            }
        });
        closeDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                closeDrawer();
            }
        });

        String dashContent = Helper.getPreference(this, Constant.DASH_CONTENT);
        Gson gson = new Gson();
        DashBoardContent dashBoardContent = gson.fromJson(dashContent,DashBoardContent.class);
        if(Helper.getPreference(this,Constant.LAST_NOTIFY).equalsIgnoreCase( String.valueOf(dashBoardContent.getLastNotificationId())))
        {
            notifyNew.setVisibility(View.INVISIBLE);
        }
        else
        {
            notifyNew.setVisibility(View.VISIBLE);
        }


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                switch (i) {
                    case 0:
                        Intent intent0 = new Intent(TestimonyActivity.this, BrowserActivity.class);
                        intent0.putExtra("url", Constant.DOWNLOADS_URL);
                        intent0.putExtra("title", Constant.DOWNLOADS);
                        startActivity(intent0);
                        break;

                    case 1:
                        Intent intent1 = new Intent(TestimonyActivity.this, RadioActivity.class);
                        startActivity(intent1);
                        break;

                    case 2:
                        Intent intent2 = new Intent(TestimonyActivity.this, BrowserActivity.class);
                        intent2.putExtra("url", Constant.ONLINE_GIVING_URL);
                        intent2.putExtra("title", Constant.ONLINE_GIVING);
                        startActivity(intent2);
                        break;
                    case 3:
                        Intent intent3 = new Intent(TestimonyActivity.this, TestimonyActivity.class);
                        startActivity(intent3);
                        break;

                    case 4:
                        Intent intent4 = new Intent(TestimonyActivity.this, BrowserActivity.class);
                        intent4.putExtra("url", Constant.ONLINE_BOOKSTORE_URL);
                        intent4.putExtra("title", Constant.ONLINE_BOOKSTORE);
                        startActivity(intent4);
                        break;

                    case 5:
                        Intent intent5 = new Intent(TestimonyActivity.this, AnnouncementActivity.class);
                        startActivity(intent5);
                        break;


                }
            }
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int lastVisibleItem, totalItemCount;
                    totalItemCount = linearLayoutManager.getItemCount();
                    int visibleThreshold = 2;
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (totalItemCount >= limit) {
                        if (!fetching && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            fabMenu.setVisibility(View.INVISIBLE);
                            loadMore.setVisibility(View.VISIBLE);
                            getTestimonies();
                        }
                    }
                }

            }
        });

    }


    public void exitApp(View v) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        // Otherwise defer to system default behavior.
        super.onBackPressed();
    }

    public void getTestimonies() {
        if (connected) {

            HttpHandler httpHandler = new HttpHandler(this);
            if (!fetching) {
                fetching = true;

                httpHandler.makeGet(Constant.BASE_URL + Constant.GET_TESTIMONIES + "?offset=" + offset + "&limit=" + limit, new RequestDoneInterface() {
                    @Override
                    public void onRequestDone(String response) {
                        Gson Converter = new Gson();
                        fetching = false;
                        Type type = new TypeToken<List<Testimony>>() {
                        }.getType();
                        try {

                            if (offset == 0) {
                                testimonies = Converter.fromJson(response, type);
                                testimonyAdapter = new TestimonyAdapter(TestimonyActivity.this, testimonies);
                                recyclerView.setAdapter(testimonyAdapter);
                                testimonyAdapter.setClickListener(TestimonyActivity.this);
                            } else {
                                ArrayList<Testimony> holder = Converter.fromJson(response, type);
                                testimonies.addAll(holder);
                                testimonyAdapter.notifyDataSetChanged();
                                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                                recyclerView.smoothScrollToPosition(linearLayoutManager.getItemCount() - holder.size());
                            }
                            offset += Constant.LIMIT;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                        loadMore.setVisibility(View.INVISIBLE);
                    }
                });

            }
        } else {
            mainContent.setVisibility(View.INVISIBLE);
            errorPage.setVisibility(View.VISIBLE);
        }

    }

    public void openDrawer(View v) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {
        drawerLayout.closeDrawer(Gravity.START);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (recyclerState != null) {
            layoutManager.onRestoreInstanceState(recyclerState);
        }
        WWWApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        recyclerState = layoutManager.onSaveInstanceState();
        state.putParcelable(M_STATE, recyclerState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if (state != null) {
            recyclerState = state.getParcelable(M_STATE);
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected = isConnected;
        mainContent.setVisibility(View.VISIBLE);
        errorPage.setVisibility(View.INVISIBLE);
    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
        return isConnected;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(new ConnectivityReceiver());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void goNotification(View v) {
        Intent intent = new Intent(this, NotificationActivity.class);
        startActivity(intent);
    }

    public void showSnack(boolean isConnected) {

        if (isConnected) {
            try {
                snackbar.dismiss();

            } catch (Exception e) {

            }

        }
        if (!isConnected) {

            final CoordinatorLayout coordinatorLayout = findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "Internet connection error", Snackbar.LENGTH_INDEFINITE);
            View snackView = snackbar.getView();
            snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.cancelled));
            TextView tv = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    public void toggleFloatMenu(View v) {

        if (!fabMenuOn) {
            showFloatMenu();
            fabMenuOn = !fabMenuOn;
        } else {
            hideFloatMenu();
            fabMenuOn = !fabMenuOn;
        }
    }

    public void showFloatMenu() {

        fabMenuView.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                fabMenuView.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(400);
        animate.setFillAfter(true);
        fabMenuView.startAnimation(animate);
    }

    public void hideFloatMenu() {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                fabMenuView.getHeight()); // toYDelta
        animate.setDuration(400);
        animate.setFillAfter(true);
        fabMenuView.startAnimation(animate);
    }

    public void goTestimony(View v) {
        closeDrawer();
        hideFloatMenu();
    }

    public void showTestimonyForm(View v) {

        Intent intent = new Intent(this,AddTestimonyActivity.class);
        startActivityForResult(intent,ADD_TESTIMONY_REQUEST);

    }


    @Override
    public void onClick(View view, int position) {

        Testimony testimony = testimonies.get(position);
        Intent intent = new Intent(this, TestimonyDetailsActivity.class);
        intent.putExtra(Constant.TESTIMONY, testimony);
        startActivity(intent);

    }


    public void goMain(View v) {

        goMain();
    }

    public void goMain() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void goAnnouncement(View v) {
        Intent intent = new Intent(this, AnnouncementActivity.class);
        startActivity(intent);
    }

    public void goParent(View v) {
        closeDrawer();
    }

    @Override
    public void onBackPressed() {
        goMain();

    }

    public void goDownloads(View v) {
        Intent intent0 = new Intent(TestimonyActivity.this, BrowserActivity.class);
        intent0.putExtra("url", Constant.DOWNLOADS_URL);
        intent0.putExtra("title", Constant.DOWNLOADS);
        startActivity(intent0);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ADD_TESTIMONY_REQUEST:
               //dO NOTHING
        }

    }


}
