package com.winnersworlwide.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.winnersworlwide.R;

public class ContactActivity extends AppCompatActivity {

    TextView navigationTitle;
    RelativeLayout navLeftView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        navigationTitle = findViewById(R.id.toolbarTitle);
        navigationTitle.setText("CONTACT");
        navLeftView = findViewById(R.id.toolbarLeftView);

        navLeftView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goParent(view);
            }
        });
    }

    public void goParent(View v) {
        goMain(v);
    }

    public void goMain(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
