package com.winnersworlwide.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.Gson;
import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.RequestDoneInterface;
import com.winnersworlwide.models.DashBoardContent;
import com.winnersworlwide.others.ConnectivityReceiver;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.services.FireBaseMessageService;
import com.winnersworlwide.others.Helper;
import com.winnersworlwide.others.HttpHandler;
import com.winnersworlwide.others.WWWApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    public String liveVideoId = "", liveVideoTitle = "";
    Snackbar snackbar;
    Boolean connected = true;
    String dashContent = "";
    ProgressBar progressBar;
    DashBoardContent dashBoardContent;
    PopupWindow updatePopup;
    View updateLayout;
    String fresher = "";
    Boolean fetching = false;
    List<String> books = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        connected = checkConnection();
        progressBar = findViewById(R.id.progressBar);
        fresher = Helper.getPreference(this, Constant.FRESHER);
        LayoutInflater layoutinflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        updateLayout = layoutinflater.inflate(R.layout.force_update_popup, null, false);
        getLiveStream();

    }


    public void getLiveStream() {

        if (connected) {
            if (!fetching) {

                fetching = true;
                String url = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + Constant.CHANNEL_ID + "&eventType=live&type=video&key=" + Constant.YOUTUBE_API_KEY;
                HttpHandler httpHandler = new HttpHandler(this);
                httpHandler.makeGet(url, new RequestDoneInterface() {
                    @Override
                    public void onRequestDone(String response) {
                        fetching = false;

                        try {


                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray Items = new JSONArray(jsonObject.getString("items"));
                            if (Items.length() > 0) {

                                JSONObject item = Items.getJSONObject(0);
                                JSONObject id = item.getJSONObject("id");
                                liveVideoId = id.getString("videoId");
                                JSONObject snippet = item.getJSONObject("snippet");
                                liveVideoTitle = snippet.getString("title");

                            }


                        } catch (JSONException e) {

                            Log.d("LIVE_ERROR", e.getMessage());
                            e.printStackTrace();
                            progressBar.setVisibility(View.INVISIBLE);


                        }

                        getDashContent();


                    }
                });
            }
        } else {
            showSnack(false);
            progressBar.setVisibility(View.INVISIBLE);
        }

    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
        return isConnected;
    }

    public void showSnack(String msg) {
        final CoordinatorLayout coordinatorLayout = findViewById(R.id.main);
        snackbar = Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_INDEFINITE);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.cancelled));
        TextView tv = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public void showSnack(boolean isConnected) {

        if (isConnected) {
            try {
                snackbar.dismiss();

            } catch (Exception e) {

            }

        }
        if (!isConnected) {

            showSnack("Connection error");
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected = isConnected;
        if (connected) {
            try {
                snackbar.dismiss();
            } catch (Exception e) {
                getLiveStream();
            }
            getLiveStream();
        }
    }

    public void goMain() {


        Helper.savePreference(SplashActivity.this, Constant.LIVE_TITLE, liveVideoTitle);
        Helper.savePreference(SplashActivity.this, Constant.LIVE_ID, liveVideoId);
        Helper.savePreference(SplashActivity.this, Constant.DASH_CONTENT, dashContent);
        Helper.savePreference(SplashActivity.this, Constant.DASH_CONTENT, dashContent);


        if (!TextUtils.isEmpty(fresher)) {
            Intent intent;
            intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            finish();

        } else {
            Intent intent1;
            FireBaseMessageService.subscribedUser();
            FireBaseMessageService.getToken();
            Helper.savePreference(SplashActivity.this, Constant.FRESHER, "WELCOME");
            intent1 = new Intent(SplashActivity.this, WelcomeActivity.class);
            startActivity(intent1);
            finish();
            //Helper.getDeviceID();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        WWWApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(new ConnectivityReceiver());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getDashContent() {

        if (connected) {
            new HttpHandler(this).makeGet(Constant.BASE_URL + Constant.GET_DASHBOARD_CONTENT, new RequestDoneInterface() {
                @Override
                public void onRequestDone(String response) {
                    try {

                        dashContent = response;
                        Gson gson = new Gson();
                        dashBoardContent = gson.fromJson(response, DashBoardContent.class);

                        if(dashBoardContent.getForceUpdate())
                        {
                            if(! dashBoardContent.getAppVersion().equalsIgnoreCase(Constant.APP_VERSION))
                            {
                                progressBar.setVisibility(View.INVISIBLE);
                                showUpdatePopUp();
                            }
                            else
                            {
                                goMain();
                            }
                        }
                        else
                        {
                           goMain();
                        }


                    } catch (Exception e) {

                        progressBar.setVisibility(View.INVISIBLE);
                        e.printStackTrace();
                        showSnack("Can't'reach Winners Engine");
                    }



                }
            },false);

        }
    }

    @Override
    public void onBackPressed() {


        finish();
        super.onBackPressed();
    }



    public void showUpdatePopUp() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        updatePopup = new PopupWindow(updateLayout, width, height, true);
        updatePopup.setAnimationStyle(android.R.style.Animation_Dialog);
        updatePopup.setOutsideTouchable(false);
        updatePopup.showAtLocation(updateLayout, Gravity.CENTER, 0, 0);
    }



    public void goPlayStore(View v)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(
                Constant.PLAYSTORE_URL));
        intent.setPackage("com.android.vending");
        startActivity(intent);
    }

}

