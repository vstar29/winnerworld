package com.winnersworlwide.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;
import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.RequestDoneInterface;
import com.winnersworlwide.others.ConnectivityReceiver;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.Helper;
import com.winnersworlwide.others.HttpHandler;
import com.winnersworlwide.others.WWWApplication;

import java.util.HashMap;
import java.util.Map;

public class AddTestimonyActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener, AdapterView.OnItemSelectedListener {

    private static final int PICK_FILE_REQUEST = 1852;
    private static final int MAX_FILE_SIZE = 2000000;
    AutoCompleteTextView nameView, titleView, testimonyView, addressView, emailView, phoneView;
    Spinner TypeView;
    CountryCodePicker ccp;
    ProgressBar addTestimonyProgress;
    String filePath = "";
    TextView attachmentName;
    Boolean sending = false;
    RelativeLayout formView;
    RelativeLayout successView;
    Boolean connected = false;
    Snackbar snackbar;
    private String type;
    TextView navigationTitle;
    RelativeLayout navLeftView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_testimony);
        nameView = findViewById(R.id.name);
        titleView = findViewById(R.id.title);
        testimonyView = findViewById(R.id.testimony);
        emailView = findViewById(R.id.email);
        phoneView = findViewById(R.id.phone);
        ccp = findViewById(R.id.ccp);
        attachmentName = findViewById(R.id.file_name);
        addressView = findViewById(R.id.address);
        TypeView = findViewById(R.id.type);
        formView = findViewById(R.id.form);
        successView = findViewById(R.id.success);
        addTestimonyProgress = findViewById(R.id.progressBar);
        navigationTitle = findViewById(R.id.toolbarTitle);
        navLeftView = findViewById(R.id.toolbarLeftView);
        navigationTitle.setText(getString(R.string.share_your_testimony));
        ccp.registerCarrierNumberEditText(phoneView);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(AddTestimonyActivity.this,
                R.array.testimony_type_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        TypeView.setAdapter(adapter);
        connected = checkConnection();
        testimonyView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getId() == R.id.testimony) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        addressView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getId() == R.id.address) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

    }

    public void submitTestimony(View v) {
        hideKeyboard();
        nameView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input));
        titleView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input));
        testimonyView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input));
        addressView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input));
        emailView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input));
        phoneView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input));
        TypeView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input));

        String name = nameView.getText().toString();
        String title = titleView.getText().toString();
        String testimony = testimonyView.getText().toString();
        String phone = ccp.getFullNumber();
        String email = emailView.getText().toString();
        String address = addressView.getText().toString();

        Boolean valid = true;
        if (!Helper.isValidName(name)) {
            nameView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input_error));
            valid = false;
        }
        if (!Helper.isValidEmail(email)) {
            emailView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input_error));
            valid = false;
        }
        if (TextUtils.isEmpty(title)) {
            titleView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input_error));
            valid = false;
        }
        if (TextUtils.isEmpty(testimony)) {
            testimonyView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input_error));
            valid = false;
        }
        if (TextUtils.isEmpty(address)) {
            addressView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input_error));
            valid = false;
        }
        if (!ccp.isValidFullNumber()) {
            phoneView.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.round_input_error));
            valid = false;
        }
        if (TextUtils.isEmpty(type)) {
            type = "OTHER";

        }
        if (valid) {
            addTestimonyProgress.setVisibility(View.VISIBLE);
            //send to server
            Map<String, String> params = new HashMap<>();
            params.put("Name", name);
            params.put("Title", title);
            params.put("Testimony", testimony);
            params.put("Phone", phone);
            params.put("Address", address);
            params.put("Email", email);
            params.put("Type", type);
            if(TextUtils.isEmpty(filePath))
            {
                params.put("Attachment", filePath);
            }


            if (connected) {
                if (!sending) {
                    sending = true;
                    HttpHandler httpHandler = new HttpHandler(this);
                    httpHandler.makePost(Constant.ADD_TESTIMONY, params, new RequestDoneInterface() {
                        @Override
                        public void onRequestDone(String response) {
                            sending = false;
                            addTestimonyProgress.setVisibility(View.INVISIBLE);
                            nameView.setText(null);
                            titleView.setText(null);
                            testimonyView.setText(null);
                            formView.setVisibility(View.INVISIBLE);
                            successView.setVisibility(View.VISIBLE);
                        }
                    });
                }

            } else {
                showSnack(false);
            }
        }

    }

    /*---hide soft keyboard---*/
    public void hideKeyboard() {

        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                hideSoftInputFromWindow(testimonyView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void selectFile(View v) {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        String extraMimeTypes[] = {"image/*", "application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeTypes);
        startActivityForResult(intent, PICK_FILE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PICK_FILE_REQUEST:
                if (resultCode == RESULT_OK) {

                    Uri file = data.getData();
                    Cursor returnCursor =
                            getContentResolver().query(file, null, null, null, null);
                    int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                    returnCursor.moveToFirst();
                    attachmentName.setText(returnCursor.getString(nameIndex));
                    if (returnCursor.getLong(sizeIndex) > MAX_FILE_SIZE) {
                        attachmentName.setText("File too large(max :2mb)");
                    }
                    else
                    {
                        filePath = file.toString();
                    }

                }
        }

    }

    public void goParent(View v) {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }





    @Override
    protected void onResume() {
        super.onResume();

        WWWApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);


    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected = isConnected;

    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
        return isConnected;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(new ConnectivityReceiver());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showSnack(boolean isConnected) {

        if (isConnected) {
            try {
                snackbar.dismiss();

            } catch (Exception e) {

            }

        }
        if (!isConnected) {

            final RelativeLayout coordinatorLayout = findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "Internet connection error", Snackbar.LENGTH_INDEFINITE);
            View snackView = snackbar.getView();
            snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.cancelled));
            TextView tv = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        type = parent.getSelectedItem().toString();
    }

    public void onNothingSelected(AdapterView<?> parent) {

    }


}
