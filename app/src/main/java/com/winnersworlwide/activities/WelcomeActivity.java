package com.winnersworlwide.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.winnersworlwide.R;
import com.winnersworlwide.adapters.TabsPagerAdapter;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.Helper;
import com.winnersworlwide.others.WWWApplication;

public class WelcomeActivity extends AppCompatActivity {

    FragmentManager mFragmentManager;
    LinearLayout slide1, slide2, slide0;
    TabsPagerAdapter adapter;
    private String liveVideoTitle, liveVideoId;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        Intent intent = getIntent();
        liveVideoId = intent.getStringExtra("LIVE_ID");
        liveVideoTitle = intent.getStringExtra("LIVE_TITLE");
        slide0 = findViewById(R.id.slide1);
        slide1 = findViewById(R.id.slide2);
        slide2 = findViewById(R.id.slide3);

        mFragmentManager = getSupportFragmentManager();
        mViewPager = findViewById(R.id.viewpager);

        adapter = new TabsPagerAdapter(mFragmentManager);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {

                mViewPager.setCurrentItem(position);
            }


            @Override
            public void onPageScrolled(int position, float arg1, int arg2) {
                switch (position) {

                    case 0:
                        slide0.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.red_circle));
                        slide1.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.silver_circle));
                        slide2.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.silver_circle));
                        break;
                    case 1:
                        slide1.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.red_circle));
                        slide0.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.silver_circle));
                        slide2.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.silver_circle));
                        break;
                    case 2:
                        slide2.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.red_circle));
                        slide0.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.silver_circle));
                        slide1.setBackground(ContextCompat.getDrawable(WWWApplication.getAppContext(), R.drawable.silver_circle));
                        break;
                    case 3:
                    default:


                }

            }

            @Override
            public void onPageScrollStateChanged(int position) {

                int curItem = mViewPager.getCurrentItem();
                int lastIdx = adapter.getCount() - 1;
                if (curItem == lastIdx && position == 1) {
                    skip();
                }
            }

        });

        mViewPager.setCurrentItem(0);
    }

    public void skip(View view) {
        skip();
    }

    public void skip() {
        if (liveVideoId == "") {

            liveVideoTitle = "Live stream offline";
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("LIVE_TITLE", liveVideoTitle);
        intent.putExtra("LIVE_ID", liveVideoId);
        startActivity(intent);
    }

    public void next(View view) {
        int currentPosition = mViewPager.getCurrentItem();
        switch (currentPosition) {
            case 0:
                mViewPager.setCurrentItem(1);
                break;
            case 1:
                mViewPager.setCurrentItem(2);
                break;
            case 2:
                skip(view);

        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        Helper.deleteFromPreference(this, Constant.LIVE_ID);
        Helper.deleteFromPreference(this, Constant.LIVE_TITLE);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);
        super.onBackPressed();

    }
}
