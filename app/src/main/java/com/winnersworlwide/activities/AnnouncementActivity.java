package com.winnersworlwide.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.winnersworlwide.R;
import com.winnersworlwide.adapters.AnnouncementAdapter;
import com.winnersworlwide.interfaces.ItemClickListener;
import com.winnersworlwide.interfaces.RequestDoneInterface;
import com.winnersworlwide.models.Announcement;
import com.winnersworlwide.others.ConnectivityReceiver;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.HttpHandler;
import com.winnersworlwide.others.WWWApplication;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnnouncementActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener, ItemClickListener {

    private static final String M_STATE = "ann_state_o";
    RecyclerView.LayoutManager layoutManager;
    Parcelable recyclerState;
    Boolean connected;
    AnnouncementAdapter announcementAdapter;
    ArrayList<Announcement> announcements;
    Snackbar snackbar;
    RelativeLayout errorPage;
    LinearLayout mainContent;
    ProgressBar progressBar;
    ProgressBar loadMore;
    Boolean fetching = false;
    TextView navigationTitle;
    RelativeLayout navLeftView;
    private RecyclerView recyclerView;
    private int offset = 0;
    private int limit = Constant.LIMIT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        navigationTitle = findViewById(R.id.toolbarTitle);
        navLeftView = findViewById(R.id.toolbarLeftView);
        navigationTitle.setText("Announcements");
        connected = checkConnection();
        progressBar = findViewById(R.id.progressBar);
        mainContent = findViewById(R.id.main_content);
        errorPage = findViewById(R.id.connection_error);
        loadMore = findViewById(R.id.loading);
        progressBar.setVisibility(View.VISIBLE);
        getAnnouncements();


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int lastVisibleItem, totalItemCount;
                    totalItemCount = linearLayoutManager.getItemCount();
                    int visibleThreshold = 2;
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (totalItemCount >= limit) {
                        if (!fetching && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            loadMore.setVisibility(View.VISIBLE);
                            getAnnouncements();
                        }
                    }
                }

            }
        });
        navLeftView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goMain(view);

            }
        });
    }

    public void goParent(View v) {
        goMain(v);
    }

    public void goMain(View v) {

        goMain();
    }

    public void goMain() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (recyclerState != null) {
            layoutManager.onRestoreInstanceState(recyclerState);
        }
        WWWApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        recyclerState = layoutManager.onSaveInstanceState();
        state.putParcelable(M_STATE, recyclerState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if (state != null) {
            recyclerState = state.getParcelable(M_STATE);
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected = isConnected;
        mainContent.setVisibility(View.VISIBLE);
        errorPage.setVisibility(View.INVISIBLE);
    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
        return isConnected;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(new ConnectivityReceiver());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showSnack(boolean isConnected) {

        if (isConnected) {
            try {
                snackbar.dismiss();

            } catch (Exception e) {

            }

        }
        if (!isConnected) {

            final CoordinatorLayout coordinatorLayout = findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "Connection Error",Snackbar.LENGTH_INDEFINITE).setAction("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            View snackView = snackbar.getView();
            snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.cancelled));
            TextView tv = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    public void getAnnouncements() {

        if (connected) {

            Map<String, String> params = new HashMap<>();
            params.put("offset", Integer.toString(offset));
            HttpHandler httpHandler = new HttpHandler(this);
            if (!fetching) {
                fetching = true;

                        httpHandler.makeGet(Constant.BASE_URL + Constant.GET_ANNOUNCEMENTS+"?offset="+offset+"&limit="+limit, new RequestDoneInterface() {
                    @Override
                    public void onRequestDone(String response) {
                        Gson Converter = new Gson();
                        fetching = false;
                        Type type = new TypeToken<List<Announcement>>() {
                        }.getType();
                        try {
                            if (offset == 0) {
                                announcements = Converter.fromJson(response, type);
                                announcementAdapter = new AnnouncementAdapter(AnnouncementActivity.this, announcements);
                                recyclerView.setAdapter(announcementAdapter);
                                announcementAdapter.setClickListener(AnnouncementActivity.this);

                            } else {
                                ArrayList<Announcement> holder = Converter.fromJson(response, type);
                                announcements.addAll(holder);
                                announcementAdapter.notifyDataSetChanged();
                                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                                recyclerView.smoothScrollToPosition(linearLayoutManager.getItemCount() - holder.size());
                            }
                            offset += Constant.LIMIT;
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                        loadMore.setVisibility(View.INVISIBLE);
                    }
                });

            }
        } else {
            mainContent.setVisibility(View.INVISIBLE);
            errorPage.setVisibility(View.VISIBLE);
        }

    }

    public void goDownloads(View v) {
        Intent intent0 = new Intent(AnnouncementActivity.this, BrowserActivity.class);
        intent0.putExtra("url", Constant.DOWNLOADS_URL);
        intent0.putExtra("title", "DOWNLOADS");
        startActivity(intent0);
    }

    @Override
    public void onClick(View view, int position) {

        Announcement announcement = announcements.get(position);
        Intent intent = new Intent(this, AnnouncementDetailsActivity.class);
        intent.putExtra(Constant.ANNOUNCEMENT, announcement);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

        goMain();

    }
}
