package com.winnersworlwide.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.RadioLoadedInterface;
import com.winnersworlwide.others.ConnectivityReceiver;
import com.winnersworlwide.services.RadioService;

public class RadioActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener, RadioLoadedInterface {

    private RadioService player;
    boolean serviceBound = false;
    Boolean connected = true;
    Snackbar snackbar;
    TextView navigationTitle;
    RelativeLayout navLeftView;
    Button playIcon;
    ProgressBar progressBar;
    private final static String SERVICE_STATE = "ServiceState";

    //Binding this Client to the RadioService Service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            RadioService.LocalBinder binder = (RadioService.LocalBinder) service;
            player = binder.getService();
            player.setLoadedInterface(RadioActivity.this);
            if(player.isPlaying())
            {
                playIcon.setBackgroundResource(R.drawable.icon_pause);
            }
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio);
        connected = checkConnection();
        navigationTitle = findViewById(R.id.toolbarTitle);
        navLeftView = findViewById(R.id.toolbarLeftView);
        navigationTitle.setText("Dominion Radio");
        playIcon = findViewById(R.id.play_icon);
        progressBar = findViewById(R.id.progressBar);
        navLeftView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goMain(view);

            }
        });


    }

    public void goParent(View v) {
        goMain(v);
    }

    public void goMain(View v) {

        goMain();
    }

    public void goMain() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected = isConnected;
        showSnack(isConnected);
    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
        return isConnected;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(new ConnectivityReceiver());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (serviceBound) {
            unbindService(serviceConnection);
            if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                player.stopSelf();
            }

        }

    }

    public void showSnack(boolean isConnected) {

        if (isConnected) {
            try {
                snackbar.dismiss();

            } catch (Exception e) {

            }

        }
        if (!isConnected) {

            final CoordinatorLayout coordinatorLayout = findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "Connection Error",Snackbar.LENGTH_INDEFINITE).setAction("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            View snackView = snackbar.getView();
            snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.cancelled));
            TextView tv = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!serviceBound) {
            Intent playerIntent = new Intent(this, RadioService.class);
            startService(playerIntent);
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
    }



    public void reload(View v)
    {

        player.startStreaming();
    }
    public void togglePlay(View view)
    {
        player.startStreaming();
        if(player.isPlaying())
        {
            playIcon.setBackgroundResource(R.drawable.icon_pause);
        }else
        {
            playIcon.setBackgroundResource(R.drawable.icon_play);
        }

    }

    public void playAudio() {

        player.playMedia();

    }

    @Override
    public void onRadioLoaded(int loaded)
    {

        switch (loaded)
        {
            case 0:
                //loading started
                progressBar.setVisibility(View.VISIBLE);
                break;
            case 1:
                //loaded ended
                progressBar.setVisibility(View.INVISIBLE);
                break;
            case 2:
                progressBar.setVisibility(View.INVISIBLE);
               if(connected)
               {
                   player.playMedia();
               }
               else
               {
                   showSnack(false);
               }
            case 3 :
                //play has started change icon pause
                playIcon.setBackgroundResource(R.drawable.icon_play);


            case 4 :
                //radio has pause chance icon play
                playIcon.setBackgroundResource(R.drawable.icon_pause);




        }


    }
    public  void pauseAudio()
    {
        player.setPause(true);
        player.pauseMedia();

    }

    public  void stopAudio(View view)
    {
        player.stopMedia();
        goMain();
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(SERVICE_STATE, serviceBound);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        serviceBound = savedInstanceState.getBoolean(SERVICE_STATE);
    }

}
