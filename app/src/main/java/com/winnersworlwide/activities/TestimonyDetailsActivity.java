package com.winnersworlwide.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.RequestDoneInterface;
import com.winnersworlwide.models.Testimony;
import com.winnersworlwide.others.ConnectivityReceiver;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.HttpHandler;
import com.winnersworlwide.others.WWWApplication;

public class TestimonyDetailsActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {


    private static final int ADD_TESTIMONY_REQUEST = 2189;
    RelativeLayout errorPage;
    LinearLayout mainContent;
    ImageView fabMenu;
    Boolean fabMenuOn = false;
    RelativeLayout navLeftView;
    TextView navigationTitle;
    Boolean connected;
    Snackbar snackbar;
    TextView testimonyDate, testimonyName, tTestimony, testimonyTitle;
    Testimony testimony;
    private RelativeLayout fabMenuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testimony_details);
        fabMenuView = findViewById(R.id.fab_menu_view);
        fabMenu = findViewById(R.id.fab_menu);
        fabMenuView.setVisibility(View.INVISIBLE);
        navLeftView = findViewById(R.id.toolbarLeftView);
        navigationTitle = findViewById(R.id.toolbarTitle);
        navigationTitle.setText("TESTIMONIES");
        mainContent = findViewById(R.id.main_content);
        errorPage = findViewById(R.id.connection_error);
        testimonyDate = findViewById(R.id.date);
        testimonyName = findViewById(R.id.name);
        testimonyTitle = findViewById(R.id.title);
        tTestimony = findViewById(R.id.testimony);
        tTestimony.setMovementMethod(new ScrollingMovementMethod());
        Intent intent = getIntent();
        testimony = intent.getParcelableExtra(Constant.TESTIMONY);
        testimonyDate.setText(testimony.getDate());
        testimonyTitle.setText(testimony.getTitle());
        tTestimony.setText(testimony.getTestimony());
        testimonyName.setText(testimony.getName());
        connected = checkConnection();
        navLeftView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goTestimony(view);
            }
        });
        increaseView();


    }

    public void goParent(View v) {
        goTestimony(v);
    }

    @Override
    protected void onResume() {
        super.onResume();
        WWWApplication.getInstance().setConnectivityListener(this);
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected = isConnected;
        mainContent.setVisibility(View.VISIBLE);
        errorPage.setVisibility(View.INVISIBLE);
    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
        return isConnected;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(new ConnectivityReceiver());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showSnack(boolean isConnected) {

        if (isConnected) {
            try {
                snackbar.dismiss();

            } catch (Exception e) {

            }

        }
        if (!isConnected) {

            final CoordinatorLayout coordinatorLayout = findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "Internet connection error", Snackbar.LENGTH_INDEFINITE);
            View snackView = snackbar.getView();
            snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.cancelled));
            TextView tv = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    public void toggleFloatMenu(View v) {

        if (!fabMenuOn) {
            showFloatMenu();
            fabMenuOn = !fabMenuOn;
        } else {
            hideFloatMenu();
            fabMenuOn = !fabMenuOn;
        }
    }

    public void showFloatMenu() {

        fabMenuView.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                fabMenuView.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(400);
        animate.setFillAfter(true);
        fabMenuView.startAnimation(animate);
    }

    public void hideFloatMenu() {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                fabMenuView.getHeight()); // toYDelta
        animate.setDuration(400);
        animate.setFillAfter(true);
        fabMenuView.startAnimation(animate);
    }

    public void increaseView() {
        new HttpHandler(this).makeGet(Constant.BASE_URL + Constant.INCREASE_VIEWS + "/" + testimony.getId(), new RequestDoneInterface() {
            @Override
            public void onRequestDone(String response) {
            }
        });
    }


    public void showTestimonyForm(View v) {

        Intent intent = new Intent(this, AddTestimonyActivity.class);
        startActivityForResult(intent, ADD_TESTIMONY_REQUEST);

    }


    public void goMain(View v) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("LIVE_TITLE", "");
        intent.putExtra("LIVE_ID", "");
        startActivity(intent);
    }

    public void goAnnouncement(View v) {

        Intent intent = new Intent(this, AnnouncementActivity.class);
        startActivity(intent);
    }

    public void goTestimony(View v) {
        Intent intent = new Intent(this, TestimonyActivity.class);
        startActivity(intent);
    }


}
