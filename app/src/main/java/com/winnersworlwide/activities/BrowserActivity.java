package com.winnersworlwide.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.winnersworlwide.R;
import com.winnersworlwide.others.ConnectivityReceiver;
import com.winnersworlwide.others.WWWApplication;

public class BrowserActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {


    String url;
    String title;
    ProgressDialog progressDialog;
    Boolean connected;
    TextView navigationTitle;
    RelativeLayout navLeftView;
    ProgressBar progressBar;
    Snackbar snackbar;
    private WebView browser;
    private Activity mActivity;
    private WebView mWebView;

    private static final int MY_PERMISSION_REQUEST_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        if (getIntent().hasExtra("url") && getIntent().hasExtra("title")) {
            url = getIntent().getStringExtra("url");
            url = url.trim();
            title = getIntent().getStringExtra("title");
        }
        mActivity = BrowserActivity.this;
        connected = checkConnection();
        browser = (WebView) findViewById(R.id.browser);
        navigationTitle = findViewById(R.id.toolbarTitle);
        navigationTitle.setText(title);
        navLeftView = findViewById(R.id.toolbarLeftView);
        progressBar = findViewById(R.id.progressBar);
        navLeftView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goMain(view);

            }
        });
        WebSettings webSettings = browser.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        browser.setWebViewClient(new WebClient());

        if (connected) {
            loadUrl();
        } else {
            showSnack(connected);
        }


        browser.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDescription,
                                        String mimetype, long contentLength) {

                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(
                        DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                String fileName = URLUtil.guessFileName(url,contentDescription,mimetype);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,fileName);

                DownloadManager dManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dManager.enqueue(request);
            }
        });

        checkPermission();


    }
    protected void checkPermission(){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                if(shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    // show an alert dialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setMessage("Write external storage permission is required.");
                    builder.setTitle("Please grant permission");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(
                                    mActivity,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    MY_PERMISSION_REQUEST_CODE
                            );
                        }
                    });
                    builder.setNeutralButton("Cancel",null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else {
                    // Request permission
                    ActivityCompat.requestPermissions(
                            mActivity,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSION_REQUEST_CODE
                    );
                }
            }else {
                // Permission already granted
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch(requestCode){
            case MY_PERMISSION_REQUEST_CODE:{
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    // Permission granted
                }else {
                    // Permission denied
                }
            }
        }
    }

    public void goParent(View v) {
        goMain();
    }

    public void loadUrl() {
        browser.loadUrl(url);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.browser_options, menu);
        return true;
    }

    public void goMain(View v) {

        goMain();
    }

    public void goMain() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void showSnack(boolean isConnected) {

        if (isConnected) {
            try {
                snackbar.dismiss();

            } catch (Exception e) {

            }

        }
        if (!isConnected) {

            final CoordinatorLayout coordinatorLayout = findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "Internet connection error", Snackbar.LENGTH_INDEFINITE);
            View snackView = snackbar.getView();
            snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.cancelled));
            TextView tv = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {

        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.openExternal) {
            Intent external = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(external);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected = isConnected;


    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
        return isConnected;
    }


    @Override
    protected void onResume() {
        super.onResume();

        WWWApplication.getInstance().setConnectivityListener(this);
    }


    private class WebClient extends WebViewClient {


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            progressDialog.show();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {

            super.onReceivedError(view, request, error);
            {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);

                } catch (Exception e) {
                    Toast.makeText(BrowserActivity.this, "Cant load url", Toast.LENGTH_LONG).show();
                }
            }

        }


        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }

       /* @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            url = url.trim();
            if (url.endsWith(".mp3") || url.endsWith(".mp4") || url.endsWith(".3gp") || url.endsWith(".pdf") || url.endsWith(".apk") || url.endsWith(".avi") || url.endsWith(".flv") || url.endsWith(".zip") || url.endsWith(".docx") || url.endsWith(".MP4")) {

                String[] splits = url.split(Pattern.quote("."));
                Uri source = Uri.parse(url);
                DownloadManager.Request request = new DownloadManager.Request(source);
                request.setDescription("Donwloading..");
                String extension = splits[splits.length - 1];

                try {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        String title = webView.getTitle() + "." + extension;
                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, title);
                        request.allowScanningByMediaScanner();
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                        manager.enqueue(request);

                    }


                } catch (Exception e) {

                }

            }
            webView.loadUrl(url);
            return true;
        }*/

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            progressDialog.dismiss();
            super.onPageFinished(view, url);
            // BrowserActivity.this.setTitle(view.getTitle());

        }
    }

}


