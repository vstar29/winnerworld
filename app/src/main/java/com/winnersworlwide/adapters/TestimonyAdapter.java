package com.winnersworlwide.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.ItemClickListener;
import com.winnersworlwide.models.Testimony;

import java.util.ArrayList;

/**
 * Created by VICTOR on 27-Feb-18.
 */

public class TestimonyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ItemClickListener clickListener;
    private ArrayList<Testimony> testimonies;


    public TestimonyAdapter(Context context, ArrayList<Testimony>
            testimonies) {
        this.context = context;
        this.testimonies = testimonies;
    }

    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return testimonies.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View relatedModel;

        relatedModel = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.testimony_model, viewGroup, false);
        return new Holder(relatedModel);


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        Holder itemHolder = (Holder) holder;
        itemHolder.testimony.setText(testimonies.get(position).getTestimony());
        itemHolder.name.setText(testimonies.get(position).getName());
        itemHolder.date.setText(testimonies.get(position).getDate());
        itemHolder.views.setText(testimonies.get(position).getDisplayViews());
        itemHolder.status.setText(testimonies.get(position).getViewStatus());
        itemHolder.avatar.setImageResource(R.drawable.default_avatar);

    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView testimony;
        TextView name;
        TextView date;
        ImageView avatar;
        TextView views;
        TextView status;

        Holder(View view) {
            super(view);
            testimony = view.findViewById(R.id.testimony);
            name = view.findViewById(R.id.name);
            date = view.findViewById(R.id.date);
            avatar = view.findViewById(R.id.avatar);
            views = view.findViewById(R.id.views);
            status = view.findViewById(R.id.status);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }

    }

}
