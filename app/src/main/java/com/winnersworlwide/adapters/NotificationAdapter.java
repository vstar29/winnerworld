package com.winnersworlwide.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.ItemClickListener;
import com.winnersworlwide.models.Notification;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.Helper;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by VICTOR on 27-Feb-18.
 */

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ItemClickListener clickListener;
    private ArrayList<Notification> notifications;


    public NotificationAdapter(Context context, ArrayList<Notification>
            notifications) {
        this.context = context;
        this.notifications = notifications;
    }

    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View relatedModel;

        relatedModel = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_model, viewGroup, false);
        return new Holder(relatedModel);


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        Notification notification = notifications.get(position);
        Holder itemHolder = (Holder) holder;

        String today =  Helper.formatTime( new Date(),"dd-MM-yyyy");


        if(position == 0)
        {
            if(Helper.formatTime( notification.getDate(),"dd-MM-yyyy") == today)
            {
                itemHolder.header.setText("Today");

            }
            else
            {
                itemHolder.header.setText(Helper.formatTime( notification.getDate(),"E dd MMMM yyyy"));
            }
        }
        else
        {
            Notification previous = notifications.get(position - 1);
            String previousDate = Helper.formatTime( previous.getDate(),"dd-MM-yyyy");
            if(Helper.formatTime( notifications.get(position).getDate(),"dd-MM-yyyy") == previousDate)
            {
                itemHolder.header.setText("");

            }
            else
            {
                itemHolder.header.setText(Helper.formatTime( notification.getDate(),"E dd MMM yyyy"));
            }
        }

        itemHolder.description.setText(notification.getDescription());
        itemHolder.title.setText(notification.getTitle());
        itemHolder.date.setText(notification.getFormattedDate());
        switch (notification.getDestination())
        {
            case Constant.ANNOUNCEMENT:
                itemHolder.icon.setImageResource(R.drawable.ic_announcements_primary);
                break;
            case Constant.ONLINE_BOOKSTORE:
                itemHolder.icon.setImageResource(R.drawable.ic_bookstore);
                break;
            default:
                itemHolder.icon.setImageResource(R.drawable.ic_announcements_primary);
                break;
        }

    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView description;
        TextView title;
        TextView date;
        ImageView icon;
        TextView header;

        Holder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
            date = view.findViewById(R.id.date);
            icon = view.findViewById(R.id.icon);
            header = view.findViewById(R.id.section_title);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }

    }

}
