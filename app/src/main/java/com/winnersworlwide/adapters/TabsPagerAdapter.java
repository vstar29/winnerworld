package com.winnersworlwide.adapters;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.winnersworlwide.fragments.WelcomeFragment;

/**
 * Created by VICTOR on 14-Oct-16.
 */


public class TabsPagerAdapter extends FragmentPagerAdapter {
    private FragmentManager fm;

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fm = fm;

    }

    @Override
    public Fragment getItem(int index) {


        switch (index) {
            case 0:

                WelcomeFragment welcomeFragment = new WelcomeFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("STAGE", 0);
                welcomeFragment.setArguments(bundle);
                return welcomeFragment;

            case 1:

                WelcomeFragment welcomeFragment1 = new WelcomeFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putInt("STAGE", 1);
                welcomeFragment1.setArguments(bundle1);
                return welcomeFragment1;
            case 2:

                WelcomeFragment welcomeFragment2 = new WelcomeFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putInt("STAGE", 2);
                welcomeFragment2.setArguments(bundle2);
                return welcomeFragment2;
            default:

                return new Fragment();
        }


    }

    @Override
    public int getCount() {

        return 3;

    }


}

