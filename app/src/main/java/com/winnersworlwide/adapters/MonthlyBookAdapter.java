package com.winnersworlwide.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.ItemClickListener;
import com.winnersworlwide.models.MonthlyBook;

import java.util.ArrayList;

/**
 * Created by VICTOR on 27-Feb-18.
 */

public class MonthlyBookAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ItemClickListener clickListener;
    private ArrayList<MonthlyBook> books;


    public MonthlyBookAdapter(Context context, ArrayList<MonthlyBook>
            books) {
        this.context = context;
        this.books = books;
    }

    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View relatedModel;

        relatedModel = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.book_model, viewGroup, false);
        return new Holder(relatedModel);


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        Holder itemHolder = (Holder) holder;
        itemHolder.author.setText(books.get(position).getAuthor());
        itemHolder.name.setText(books.get(position).getBook());


    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView author;
        TextView name;

        Holder(View view) {
            super(view);
            author = view.findViewById(R.id.testimony);
            name = view.findViewById(R.id.name);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }

    }

}
