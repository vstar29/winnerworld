package com.winnersworlwide.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.ItemClickListener;
import com.winnersworlwide.models.Announcement;
import com.winnersworlwide.others.Helper;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by VICTOR on 27-Feb-18.
 */

public class AnnouncementAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater layoutinflater;
    private ItemClickListener clickListener;
    private ArrayList<Announcement> announcements;


    public AnnouncementAdapter(Context context, ArrayList<Announcement>
            announcements) {
        this.context = context;
        layoutinflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.announcements = announcements;
    }

    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return announcements.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View relatedModel;

        relatedModel = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.announcement_model, viewGroup, false);
        return new Holder(relatedModel);


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        Holder itemHolder = (Holder) holder;
        String today =  Helper.formatTime( new Date(),"dd-MM-yyyy");


        if(position == 0)
        {
            if(Helper.formatTime( announcements.get(position).getDate(),"dd-MM-yyyy") == today)
            {
                itemHolder.header.setText("Today");

            }
            else
            {
                itemHolder.header.setText(Helper.formatTime( announcements.get(position).getDate(),"E dd MMMM yyyy"));
            }
        }
        else
        {
            Announcement previous = announcements.get(position - 1);
            String previousDate = Helper.formatTime( previous.getDate(),"dd-MM-yyyy");
            if(Helper.formatTime( announcements.get(position).getDate(),"dd-MM-yyyy") == previousDate)
            {
                itemHolder.header.setText("");

            }
            else
            {
                itemHolder.header.setText(Helper.formatTime( announcements.get(position).getDate(),"E dd MMM yyyy"));
            }
        }

        itemHolder.title.setText(announcements.get(position).getTitle());
        itemHolder.date.setText(Helper.formatTime( announcements.get(position).getDate(),"dd MMM yyyy | hh mm a"));


    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView title;
        TextView header;
        TextView date;

        Holder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            date = view.findViewById(R.id.date);
            header = view.findViewById(R.id.section_title);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }

    }

}
