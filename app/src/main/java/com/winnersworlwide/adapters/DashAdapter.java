package com.winnersworlwide.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.ItemClickListener;
import com.winnersworlwide.models.DashItem;

import java.util.ArrayList;

/**
 * Created by VICTOR on 27-Feb-18.
 */

public class DashAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater layoutinflater;
    private ItemClickListener clickListener;
    private ArrayList<DashItem> dashItems;


    public DashAdapter(Context context, ArrayList<DashItem>
            dashItems) {
        this.context = context;
        layoutinflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.dashItems = dashItems;
    }

    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return dashItems.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View relatedModel;

        relatedModel = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dash_item_model, viewGroup, false);
        return new Holder(relatedModel);


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        Holder itemHolder = (Holder) holder;
        itemHolder.title.setText(dashItems.get(position).getTitle());
        itemHolder.excerpt.setText(dashItems.get(position).getExcerpt());
        itemHolder.verse.setText(dashItems.get(position).getVerse());
        itemHolder.icon.setImageResource(dashItems.get(position).getResourceId());

    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView title;
        TextView excerpt;
        TextView verse;
        ImageView icon;

        Holder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            excerpt = view.findViewById(R.id.excerpt);
            verse = view.findViewById(R.id.verse);
            icon = view.findViewById(R.id.icon);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }


    }

}
