package com.winnersworlwide.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winnersworlwide.R;


/**
 * Created by JUMIA-1888 on 2/19/2018.
 */

public class DrawerItemAdapter extends BaseAdapter {

    LayoutInflater layoutInflater;
    private TypedArray icons;
    private Context context;
    private String[] name;

    public DrawerItemAdapter(Context context) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.icons = context.getResources().obtainTypedArray(R.array.drawer_item_icon);
        Resources resource = context.getResources();
        this.name = resource.getStringArray(R.array.drawer_item_name);
    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder listViewHolder;
        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.drawer_item_model, parent, false);
            listViewHolder = new ViewHolder();
            listViewHolder.title = convertView.findViewById(R.id.title);
            listViewHolder.icon = convertView.findViewById(R.id.icon);
            listViewHolder.count = convertView.findViewById(R.id.count);

            convertView.setTag(listViewHolder);
        } else {
            listViewHolder = (ViewHolder) convertView.getTag();
        }

       /* if (position == 1) {
            listViewHolder.count.setVisibility(View.VISIBLE);
        } else {
            listViewHolder.count.setVisibility(View.INVISIBLE);
        }*/
        listViewHolder.title.setText(name[position]);
        listViewHolder.icon.setImageResource(icons.getResourceId(position, R.drawable.ic_contact));
        return convertView;
    }

    static class ViewHolder {

        ImageView icon;
        TextView title;
        TextView count;

    }

}
