package com.winnersworlwide.models;

import android.os.Parcel;
import android.os.Parcelable;

public class MonthlyBook implements Parcelable {

    public static final Creator<MonthlyBook> CREATOR
            = new Creator<MonthlyBook>() {
        // @Override
        public MonthlyBook createFromParcel(Parcel in) {
            return new MonthlyBook(in);
        }

        // @Override
        public MonthlyBook[] newArray(int size) {
            return new MonthlyBook[size];

        }
    };
    private int ID;
    private String Book;
    private String Author;
    private String Period;



    public MonthlyBook(int id, String book, String author,String period) {
        this.ID = id;
        this.Book = book;
        this.Author = author;
        this.Period = period;

    }

    public MonthlyBook(Parcel in) {

        ID = in.readInt();
        Book = in.readString();
        Author = in.readString();
        Period = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeString(Book);
        parcel.writeString(Author);
        parcel.writeString(Period);

    }

    public int getID() {
        return ID;
    }

    public String getBook() {
        return Book;
    }

    public String getAuthor() {
        return Author;
    }

    public String getPeriod() {
        return Period;
    }
}

