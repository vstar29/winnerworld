package com.winnersworlwide.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;

import com.winnersworlwide.others.Helper;

public class Announcement implements Parcelable {

    public static final Parcelable.Creator<Announcement> CREATOR
            = new Parcelable.Creator<Announcement>() {
        // @Override
        public Announcement createFromParcel(Parcel in) {
            return new Announcement(in);
        }

        // @Override
        public Announcement[] newArray(int size) {
            return new Announcement[size];

        }
    };
    public String Description;
    private int ID;
    private String Title;
    private String Attachment;
    private String DateCreated;
    private String FeaturedImage;

    public Announcement(int id, String title, String attachment, String description, String date) {
        this.ID = id;
        this.Title = title;
        this.Attachment = attachment;
        this.Description = description;
        this.DateCreated = date;
    }

    public Announcement(Parcel in) {

        ID = in.readInt();
        Title = in.readString();
        Attachment = in.readString();
        Description = in.readString();
        DateCreated = in.readString();
    }

    public int getId() {
        return ID;
    }

    public String getTitle() {
        return Title;
    }

    public String getFormattedDate() {
        return Helper.formatTime(DateCreated, "dd-MM-yyyy | hh mm a");
    }

    public String getAttachment() {
        return Attachment;
    }

    public String getDescription() {
        String description = Description.replace("<br>","");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(description, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(description).toString();
        }
    }

    public String getFeaturedImage() {
        return FeaturedImage;
    }

    public String getDate() {
        return DateCreated;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeString(Title);
        parcel.writeString(Attachment);
        parcel.writeString(Description);
        parcel.writeString(DateCreated);
    }


}
