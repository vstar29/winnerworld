package com.winnersworlwide.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.winnersworlwide.others.Helper;

public class MonthlyTheme implements Parcelable {

    public static final Creator<MonthlyTheme> CREATOR
            = new Creator<MonthlyTheme>() {
        // @Override
        public MonthlyTheme createFromParcel(Parcel in) {
            return new MonthlyTheme(in);
        }

        // @Override
        public MonthlyTheme[] newArray(int size) {
            return new MonthlyTheme[size];

        }
    };
    private int ID;
    private String Theme;
    private String Period;
    private String Scripture;

    public MonthlyTheme(int id, String theme, String period, String Scripture) {
        this.ID = id;
        this.Theme = theme;
        this.Period = period;
        this.Scripture = Scripture;

    }

    public MonthlyTheme(Parcel in) {

        ID = in.readInt();
        Theme = in.readString();
        Period = in.readString();
        Scripture = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeString(Theme);
        parcel.writeString(Period);
        parcel.writeString(Scripture);

    }

    public int getId() {
        return ID;
    }

    public void setId(int id) {
        this.ID = id;
    }

    public String getPeriod() {
        return Helper.formatTime(Period, "MMMM yyyy");
    }

    public String getScripture() {
        return Scripture;
    }

    public String getTitle() {
        return  "Theme Of The Month (" + getPeriod() + ")";
    }

    public String getTheme() {
        return Theme;
    }
}
