package com.winnersworlwide.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class DashBoardContent implements Parcelable {

    private String AppVersion;
    private Boolean ForceUpdate;
    private ArrayList<MonthlyBook> BooksOfTheMonth;
    private MonthlyTheme ThemeOfTheMonth;
    private int LastNotificationId;

    public DashBoardContent(String appVersion, Boolean forceUpdate, ArrayList<MonthlyBook> booksOfTheMonth, MonthlyTheme themeOfTheMonth, int lastNotificationId) {
        AppVersion = appVersion;
        ForceUpdate = forceUpdate;
        BooksOfTheMonth = booksOfTheMonth;
        ThemeOfTheMonth = themeOfTheMonth;
        LastNotificationId = lastNotificationId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(AppVersion);
        parcel.writeByte((byte) (ForceUpdate ? 1 : 0));
        parcel.writeTypedList(BooksOfTheMonth);
        parcel.writeParcelable(ThemeOfTheMonth,i);
        parcel.writeInt(LastNotificationId);
    }

    public ArrayList<MonthlyBook> getBooksofTheMonth()
    {
       return this.BooksOfTheMonth;

    }


    public String getAppVersion() {
        return AppVersion;
    }

    public Boolean getForceUpdate() {
        return ForceUpdate;
    }

    public ArrayList<MonthlyBook> getBooksOfTheMonth() {
        return BooksOfTheMonth;
    }

    public MonthlyTheme getThemeOfTheMonth() {
        return ThemeOfTheMonth;
    }

    public int getLastNotificationId() {
        return LastNotificationId;
    }

    public static final Creator<DashBoardContent> CREATOR
            = new Creator<DashBoardContent>() {
        // @Override
        public DashBoardContent createFromParcel(Parcel in) {
            return new DashBoardContent(in);
        }

        // @Override
        public DashBoardContent[] newArray(int size) {
            return new DashBoardContent[size];

        }
    };

    public DashBoardContent(Parcel in) {

        AppVersion = in.readString();
        ForceUpdate = in.readByte()!=0;
        in.readTypedList(this.BooksOfTheMonth, MonthlyBook.CREATOR);
        ThemeOfTheMonth = in.readParcelable(MonthlyTheme.class.getClassLoader());
        LastNotificationId = in.readInt();
    }





}
