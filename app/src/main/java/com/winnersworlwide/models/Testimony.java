package com.winnersworlwide.models;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;

import com.winnersworlwide.others.Helper;

public class Testimony implements Parcelable {

    public static final Parcelable.Creator<Testimony> CREATOR
            = new Parcelable.Creator<Testimony>() {
        // @Override
        public Testimony createFromParcel(Parcel in) {
            return new Testimony(in);
        }

        // @Override
        public Testimony[] newArray(int size) {
            return new Testimony[size];

        }
    };
    private int ID;
    private String Name;
    private String Title;
    private String Testimony;
    private int Views;
    private Boolean Featured;
    private String Address;
    private String Email;
    private String Phone;
    private String DateCreated;


    public Testimony(int id, String name, String title, String testimony, String date, int views,Boolean featured, String address, String email,String phone,String attachement) {
        this.ID = id;
        this.Name = name;
        this.Title = title;
        this.Testimony = testimony;
        this.DateCreated = date;
        this.Views = views;
        this.Featured = featured;
        this.Address = address;
        this.Phone = phone;
        this.Email = email;


    }

    public Testimony(Parcel in) {

        ID = in.readInt();
        Name = in.readString();
        Title = in.readString();
        Testimony = in.readString();
        DateCreated = in.readString();
        Views = in.readInt();
        Featured = in.readByte() != 0;
        Address = in.readString();
        Phone = in.readString();
        Email = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeString(Name);
        parcel.writeString(Title);
        parcel.writeString(Testimony);
        parcel.writeString(DateCreated);
        parcel.writeInt(Views);
        parcel.writeByte((byte) (Featured ? 1 : 0));
        parcel.writeString(Address);
        parcel.writeString(Phone);
        parcel.writeString(Email);
    }

    public int getId() {
        return ID;
    }

    public void setId(int id) {
        this.ID = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        this.Title = title;
    }

    public Boolean getFeatured() {
        return Featured;
    }

    public String getAddress() {
        return Address;
    }

    public String getEmail() {
        return Email;
    }

    public String getPhone() {
        return Phone;
    }

    public  String getViewStatus()
    {
        return (getFeatured() == true)? "Old" :"New";
    }

    @SuppressWarnings("deprecation")
    public String getTestimony() {

        String testimony = this.Testimony.replace("<br>","");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(testimony, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(testimony).toString();
        }
    }

    public void setTestimony(String testimony) {
        this.Testimony = testimony;
    }

    public String getDate() {
        return Helper.formatTime(DateCreated, "dd-MM-yyyy | hh mm a");
    }

    public int getViews() {
        return Views;
    }
    public String getDisplayViews() {
        return Views+ " Views";
    }
}
