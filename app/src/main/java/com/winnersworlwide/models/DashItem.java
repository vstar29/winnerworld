package com.winnersworlwide.models;

public class DashItem {

    private String title;
    private String excerpt;
    private int resourceId;
    private String verse;

    public DashItem(String title, String excerpt, int resourceId, String verse) {
        this.title = title;
        this.excerpt = excerpt;
        this.resourceId = resourceId;
        this.verse = verse;
    }

    public String getTitle() {
        return title;
    }

    public int getResourceId() {
        return resourceId;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public String getVerse() {
        return verse;
    }
}
