package com.winnersworlwide.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;

import com.winnersworlwide.others.Helper;

public class Notification implements Parcelable {

    public static final Creator<Notification> CREATOR
            = new Creator<Notification>() {
        // @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        // @Override
        public Notification[] newArray(int size) {
            return new Notification[size];

        }
    };

    private int ID;
    private String Title;
    private int DestinationID;
    private String Destination;
    public String Description;
    private String DateCreated;
    private String Status;


    public Notification(int id, String title, String destination, int destinationId, String description, String date, String status) {
        this.ID = id;
        this.Title = title;
        this.DestinationID = destinationId;
        this.Destination  = destination;
        this.Description = description;
        this.DateCreated = date;
        this.Status = status;
    }

    public Notification(Parcel in) {

        ID = in.readInt();
        Title = in.readString();
        DestinationID = in.readInt();
        Destination = in.readString();
        Description = in.readString();
        DateCreated = in.readString();
        Status = in.readString();
    }

    public int getId() {
        return ID;
    }

    public String getTitle() {
        return Title;
    }

    public String getFormattedDate() {
        return Helper.formatTime(DateCreated, "hh : mma");
    }

    public String getDestination() {
        return Destination;
    }

    public String getDescription() {
        String description = Description.replace("<br>","");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(this.Description, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(this.Description).toString();
        }
    }

    public int getDestinationID() {
        return DestinationID;
    }

    public String getDate() {
        return DateCreated;
    }

    public String getStatus() {
        return Status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeString(Title);
        parcel.writeInt(DestinationID);
        parcel.writeString(Destination);
        parcel.writeString(Description);
        parcel.writeString(DateCreated);
        parcel.writeString(Status);
    }


}
