package com.winnersworlwide.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.session.MediaSessionManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.RemoteException;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.winnersworlwide.R;
import com.winnersworlwide.interfaces.RadioLoadedInterface;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.PlaybackStatus;

import java.io.IOException;

import androidx.core.app.NotificationCompat;

public class RadioService extends Service implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener,

        AudioManager.OnAudioFocusChangeListener {

    public static final String ACTION_PLAY = "com.winners.app.audioplayer.ACTION_PLAY";
    public static final String ACTION_PAUSE = "com.winners.app.valdio.audioplayer.ACTION_PAUSE";
    public static final String ACTION_STOP = "com.winners.app.valdio.audioplayer.ACTION_STOP";
    //AudioPlayer notification ID
    private static final int NOTIFICATION_ID = 101;
    private final IBinder iBinder = new LocalBinder();
    Boolean loaded = false;
    AudioManager audioManager;
    Boolean playRequested = false;
    private MediaPlayer player;
    private int resumePosition = 0;
    private boolean ongoingCall = false;
    private boolean pause = false;
    private PhoneStateListener phoneStateListener;
    private TelephonyManager telephonyManager;
    private RadioLoadedInterface radioLoadedInterface;
    //MediaSession
    private MediaSessionManager mediaSessionManager;
    private MediaSessionCompat mediaSession;
    private MediaControllerCompat.TransportControls transportControls;
    private BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //pause audio on ACTION_AUDIO_BECOMING_NOISY
            pauseMedia();
            buildNotification(PlaybackStatus.PAUSED);
        }
    };

    public void onCreate() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stopMedia();
        stopSelf();
    }

    //Handle errors
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        loaded = false;
        //Invoked when there has been an error during an asynchronou
        // s operation.
        if (radioLoadedInterface != null) {
            radioLoadedInterface.onRadioLoaded(2);
        }
        return false;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {

        loaded = true;
        if (playRequested) {
            playMedia();
        }
        if (radioLoadedInterface != null) {
            radioLoadedInterface.onRadioLoaded(1);
        }
    }

    public void initMusicPlayer() {
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

    }

    public void prepareStreaming() {

        if (radioLoadedInterface != null) {
            radioLoadedInterface.onRadioLoaded(0);
        }
        player = new MediaPlayer();
        //Set up player event listeners
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
        player.setOnPreparedListener(this);
        player.setOnBufferingUpdateListener(this);
        player.setOnInfoListener(this);
        //Reset so that the player is not pointing to another data source
        player.reset();

        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            // Set the data source to the mediaFile location
            player.setDataSource(RadioService.this, Uri.parse(Constant.DOMI_RADIO));
        } catch (IOException e) {
            e.printStackTrace();
            if (radioLoadedInterface != null) {
                radioLoadedInterface.onRadioLoaded(2);
            }
            stopSelf();
        }
        player.prepareAsync();

    }

    public void startStreaming() {


        if (player == null || !loaded) {
            playRequested = true;
            prepareStreaming();
        } else {
            if (player.isPlaying()) {
                pauseMedia();
                if (radioLoadedInterface != null) {

                }

            } else if (loaded) {
                playMedia();
                if (radioLoadedInterface != null) {
                    radioLoadedInterface.onRadioLoaded(1);
                }

            }
        }

    }


    public void stopMedia() {
        if (player == null) return;
        if (player.isPlaying()) {
            player.stop();
            player.release();
        }
        loaded = false;

    }

    public void pauseMedia() {
        if (player == null) {
            playRequested = true;
            prepareStreaming();
        } else {
            if (player.isPlaying()) {

                player.pause();
                resumePosition = player.getCurrentPosition();
            }
        }
        if(radioLoadedInterface != null)
        {
            radioLoadedInterface.onRadioLoaded(3);
        }
        buildNotification(PlaybackStatus.PAUSED);


    }

    public void playMedia() {

        pause = false;
        if (player == null) {
            playRequested = true;
            prepareStreaming();
        } else {
            if (!player.isPlaying()) {
                if (loaded) {
                    player.start();
                }

            }
        }
        if(radioLoadedInterface != null)
        {
            radioLoadedInterface.onRadioLoaded(4);
        }
        buildNotification(PlaybackStatus.PLAYING);

    }

    public void setPause(Boolean pauseState) {
        pause = pauseState;
    }

    private void resumeMedia() {

        playMedia();

    }

    public Boolean isPlaying() {
        if (player == null) {
            return false;
        }
        return player.isPlaying();

    }

    public Boolean isLoaded() {
        return loaded;
    }

    public void setLoadedInterface(RadioLoadedInterface radioLoadedInterface) {
        this.radioLoadedInterface = radioLoadedInterface;
    }

    @Override
    public void onAudioFocusChange(int focusState) {
        //Invoked when the audio focus of the system is updated.
        switch (focusState) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback

                if (player != null) {
                    player.setVolume(1.0f, 1.0f);
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and release media player
                if (player.isPlaying()) player.stop();
                player.release();
                player = null;
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (player.isPlaying()) player.pause();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (player.isPlaying()) player.setVolume(0.1f, 0.1f);
                break;
        }
    }

    private boolean requestAudioFocus() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            //Focus gained
            return true;
        }
        //Could not gain focus
        return false;
    }

    private boolean removeAudioFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                audioManager.abandonAudioFocus(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Request audio focus
        if (requestAudioFocus() == false) {
            //Could not gain focus
            stopSelf();
        }
        if (mediaSessionManager == null) {
            try {

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    initMediaSession();
                }

                prepareStreaming();
            } catch (RemoteException e) {
                e.printStackTrace();

                stopSelf();
            }
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                buildNotification(PlaybackStatus.PAUSED);
            }

        }

        //Handle Intent action from MediaSession.TransportControls
        handleIncomingActions(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player != null) {
            stopMedia();

        }
        removeAudioFocus();
        //Disable the PhoneStateListener
        if (phoneStateListener != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }

        removeNotification();
    }

    private void registerBecomingNoisyReceiver() {
        //register after getting audio focus
        IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(becomingNoisyReceiver, intentFilter);
    }


    //Handle incoming phone calls
    private void callStateListener() {
        // Get the telephony manager
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        //Starting listening for PhoneState changes
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    //if at least one call exists or the phone is ringing
                    //pause the MediaPlayer
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (player != null) {
                            pauseMedia();
                            ongoingCall = true;
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        // Phone idle. Start playing.
                        if (player != null) {
                            if (ongoingCall) {
                                ongoingCall = false;
                                resumeMedia();
                            }
                        }
                        break;
                }
            }
        };
        // Register the listener with the telephony manager
        // Listen for changes to the device call state.
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }


    private void initMediaSession() throws RemoteException {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {


            if (mediaSessionManager != null) return; //mediaSessionManager exists

            mediaSessionManager = (MediaSessionManager) getSystemService(Context.MEDIA_SESSION_SERVICE);
            // Create a new MediaSession
            mediaSession = new MediaSessionCompat(getApplicationContext(), "AudioPlayer");
            //Get MediaSessions transport controls
            transportControls = mediaSession.getController().getTransportControls();
            //set MediaSession -> ready to receive media commands
            mediaSession.setActive(true);
            //indicate that the MediaSession handles transport control commands
            // through its MediaSessionCompat.Callback.
            mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);

            // Attach Callback to receive MediaSession updates
            mediaSession.setCallback(new MediaSessionCompat.Callback() {
                // Implement callbacks
                @Override
                public void onPlay() {
                    super.onPlay();
                    resumeMedia();
                    buildNotification(PlaybackStatus.PLAYING);
                }

                @Override
                public void onPause() {
                    super.onPause();
                    pause = true;
                    pauseMedia();
                    buildNotification(PlaybackStatus.PAUSED);
                }

                @Override
                public void onStop() {
                    super.onStop();
                    loaded = false;
                    removeNotification();
                    stopMedia();
                    //Stop the service
                    stopSelf();

                }

                @Override
                public void onSeekTo(long position) {
                    super.onSeekTo(position);
                }
            });

        }

    }


    private void buildNotification(PlaybackStatus playbackStatus) {

        int notificationAction = android.R.drawable.ic_media_play;//needs to be initialized
        PendingIntent play_pauseAction = null;

        //Build a new notification according to the current state of the MediaPlayer
        if (playbackStatus == PlaybackStatus.PLAYING) {
            notificationAction = android.R.drawable.ic_media_pause;
            //create the pause action
            play_pauseAction = playbackAction(1);
        } else if (playbackStatus == PlaybackStatus.PAUSED) {
            notificationAction = android.R.drawable.ic_media_play;
            //create the play action
            play_pauseAction = playbackAction(0);
        }
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),
                R.drawable.oyedepo); //replace with your own image

        // Create a new Notification


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, Constant.RADIO_NOTIFY_ID)
                .setShowWhen(false)
                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                        // Attach our MediaSession token
                        .setMediaSession(mediaSession.getSessionToken())
                        // Show our playback controls in the compact notification view.
                        .setShowActionsInCompactView(0, 1))
                // Set the Notification color
                .setColor(getResources().getColor(R.color.colorPrimary))
                // Set the large and small icons
                .setLargeIcon(largeIcon)
                .setSmallIcon(R.drawable.winners_logo)
                // Set Notification content information
                .setContentText(" WINNERS WORLD ")
                .setContentTitle("Domi Radio")

                // Add playback actions.addAction(notificationAction, "play", play_pauseAction)
                .addAction(notificationAction, "pause", play_pauseAction)
                .addAction(R.drawable.icon_quit, "stop", playbackAction(2))
                .setOngoing(true);

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(NOTIFICATION_ID, notificationBuilder.build());
    }



    private void removeNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    private PendingIntent playbackAction(int actionNumber) {
        Intent playbackAction = new Intent(this, RadioService.class);
        switch (actionNumber) {
            case 0:
                // Play
                playbackAction.setAction(ACTION_PLAY);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 1:
                // Pause
                playbackAction.setAction(ACTION_PAUSE);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 2:
                // Stop
                playbackAction.setAction(ACTION_STOP);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);

            default:
                break;
        }
        return null;
    }

    private void handleIncomingActions(Intent playbackAction) {
        if (playbackAction == null || playbackAction.getAction() == null) return;

        String actionString = playbackAction.getAction();
        if (actionString.equalsIgnoreCase(ACTION_PLAY)) {
            transportControls.play();
        } else if (actionString.equalsIgnoreCase(ACTION_PAUSE)) {
            transportControls.pause();
        } else if (actionString.equalsIgnoreCase(ACTION_STOP)) {
            transportControls.stop();
        }
    }


    public class LocalBinder extends Binder {
        public RadioService getService() {
            return RadioService.this;
        }
    }
}



