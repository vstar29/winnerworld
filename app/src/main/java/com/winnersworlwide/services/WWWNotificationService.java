package com.winnersworlwide.services;

import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import com.google.gson.Gson;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationReceivedResult;
import com.winnersworlwide.models.Notification;
import org.json.JSONException;
import org.json.JSONObject;


public class WWWNotificationService extends NotificationExtenderService {


    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
        JSONObject data = receivedResult.payload.additionalData;
        Gson gson = new Gson();
        String notificationObject = "EMPTY";
        try {
            notificationObject = data.getString("notificationObject");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Notification notification = gson.fromJson(notificationObject, Notification.class);
        OverrideSettings overrideSettings = new OverrideSettings();
        overrideSettings.extender = new NotificationCompat.Extender() {
            @Override
            public NotificationCompat.Builder extend(NotificationCompat.Builder builder) {
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                return builder
                        .setContentText(notification.getDescription())
                        .setContentText(notification.getTitle())
                        .setSound(defaultSoundUri);

            }
        };

        OSNotificationDisplayedResult displayedResult = displayNotification(overrideSettings);
        return false;
    }

}
