package com.winnersworlwide.services;


import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.winnersworlwide.interfaces.RequestDoneInterface;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.Helper;
import com.winnersworlwide.others.HttpHandler;
import com.winnersworlwide.others.WWWApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Vstar on 01/21/2019.
 */

public class StreamSynchService extends Service {

    public static final long NOTIFY_INTERVAL = 300000; //5mins

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;
    private String liveVideoId = "" ,liveVideoTitle = "";


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        // cancel if already existed
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        // schedule task
        mTimer.scheduleAtFixedRate(new DataSynchTask(), 0, NOTIFY_INTERVAL);
    }

    private void broadCastStream() {

        Intent intent = new Intent(Constant.STREAM_EVENT);
        intent.putExtra(Constant.LIVE_TITLE,String.valueOf(liveVideoTitle));
        intent.putExtra(Constant.LIVE_ID, String.valueOf(liveVideoId));
        if(liveVideoId != Helper.getPreference(WWWApplication.getAppContext(),Constant.LIVE_ID))
        {

            Helper.savePreference(WWWApplication.getAppContext(), Constant.LIVE_TITLE, liveVideoTitle);
            Helper.savePreference(WWWApplication.getAppContext(), Constant.LIVE_ID, liveVideoId);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }

    }

    class DataSynchTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    syncData();

                }

            });
        }

        public void syncData() {

            String url = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + Constant.CHANNEL_ID + "&eventType=live&type=video&key=" + Constant.YOUTUBE_API_KEY;
            HttpHandler httpHandler = new HttpHandler(WWWApplication.getAppContext());
            httpHandler.makeGet(url, new RequestDoneInterface() {
                @Override
                public void onRequestDone(String response) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray Items = new JSONArray(jsonObject.getString("items"));
                        if (Items.length() > 0) {

                            JSONObject item = Items.getJSONObject(0);
                            JSONObject id = item.getJSONObject("id");
                            liveVideoId = id.getString("videoId");
                            JSONObject snippet = item.getJSONObject("snippet");
                            liveVideoTitle = snippet.getString("title");

                            if (!TextUtils.isEmpty(liveVideoId)) {
                               broadCastStream();
                            }

                        }


                    } catch (JSONException e) {
                        Log.d("LIVE_ERROR", e.getMessage());
                        e.printStackTrace();

                    }
                }
            });

        }
    }
}
