package com.winnersworlwide.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.winnersworlwide.R;
import com.winnersworlwide.activities.MainActivity;
import com.winnersworlwide.models.Notification;
import com.winnersworlwide.others.Constant;
import com.winnersworlwide.others.WWWApplication;

/**
 * Created by VICTOR on 15-Nov-16.
 */
public class FireBaseMessageService extends FirebaseMessagingService {



    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("FIRE_TOKEN","FIRE_TOKEN:"+s);
        subscribedUser();

    }

    public static  void getToken()
    {

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        String token = task.getResult().getToken();

                    }
                });

    }

    public static void subscribedUser() {


        FirebaseMessaging.getInstance().subscribeToTopic("all")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {


                    }
                });
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Gson gson = new Gson();
        try {
            String notificationString = remoteMessage.getData().get("notificationObject");
            Log.d("NOTIFY_OBJ","NOTIFY_OBJ:"+notificationString);
            Notification notification = gson.fromJson(notificationString, Notification.class);
            sendNotification(notification,notificationString);

        } catch (Exception e) {
            e.printStackTrace();


        }

    }

    private Intent getPendingIntent()
    {

        PackageManager pm = getPackageManager();
        Intent launchIntent = pm.getLaunchIntentForPackage("com.winnersworlwide");
        return launchIntent;
    }

    private void sendNotification(Notification notification, String notifyString) {

        Intent intent = new Intent(this, MainActivity.class);
        //Intent intent = getPendingIntent();

        intent.putExtra("notifyCallBack",notification);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(WWWApplication.getAppContext(),0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, Constant.FCM_CHANNEL_ID)

                .setSmallIcon(R.drawable.winners_logo)
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getDescription())
                .setAutoCancel(false)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 , notificationBuilder.build());

    }





}
